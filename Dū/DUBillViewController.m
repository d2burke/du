//
//  DUBillViewController.m
//  Dū
//
//  Created by Daniel Burke on 12/13/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "DUBillViewController.h"
#import "UIImage+ImageEffects.h"

@interface DUBillViewController ()

@end

@implementation DUBillViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initNav];
    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}


//INITIATE UI
-(void)initNav{
    self.navigationItem.hidesBackButton = YES;
    
    UIView *navContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIImage *navLogoImage = [UIImage imageNamed:@"logo_nav_button"];
    UIImage *navLogoImageDown = [UIImage imageNamed:@"logo_nav_button"];
    UIButton *navLogo = [[UIButton alloc] initWithFrame:CGRectMake(132, 0, 44, 44)];
    navLogo.tintColor = [UIColor grayColor];
    [navLogo setBackgroundImage:navLogoImage forState:UIControlStateNormal];
    [navLogo setBackgroundImage:navLogoImageDown forState:UIControlStateHighlighted];
    //    [navLogo addTarget:self action:@selector(showPopOver:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:navLogo];
    
    UIImage *backImage = [UIImage imageNamed:@"back_nav_button"];
    UIImage *backImageDown = [UIImage imageNamed:@"back_nav_button"];
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0-5, 0, 44, 44)];
    backBtn.tintColor = [UIColor grayColor];
    [backBtn setBackgroundImage:backImage forState:UIControlStateNormal];
    [backBtn setBackgroundImage:backImageDown forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:backBtn];
    
    UIImage *reuseImage = [UIImage imageNamed:@"reuse_nav_button"];
    UIImage *reuseImageDown = [UIImage imageNamed:@"reuse_nav_button"];
    UIButton *reuseBtn = [[UIButton alloc] initWithFrame:CGRectMake(265, 0, 44, 44)];
    reuseBtn.tintColor = [UIColor grayColor];
    [reuseBtn setBackgroundImage:reuseImage forState:UIControlStateNormal];
    [reuseBtn setBackgroundImage:reuseImageDown forState:UIControlStateHighlighted];
    //    [reuseBtn addTarget:self action:@selector(createTopic:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:reuseBtn];
    
    
    UIImage *searchImage = [UIImage imageNamed:@"search_nav_button"];
    UIImage *searchImageDown = [UIImage imageNamed:@"search_nav_button"];
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(44, 0, 44, 44)];
    searchBtn.tintColor = [UIColor grayColor];
    [searchBtn setBackgroundImage:searchImage forState:UIControlStateNormal];
    [searchBtn setBackgroundImage:searchImageDown forState:UIControlStateHighlighted];
    //    [searchBtn addTarget:self action:@selector(viewProfile:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:searchBtn];
    
    [self.navigationItem setTitleView:navContainer];
}

-(void)initUI{
    
    _bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nz"]];
    _bg.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    _bg.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_bg];
    
    UIGraphicsBeginImageContextWithOptions(_bg.frame.size, NO, 0);
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, _bg.frame.size.width, _bg.frame.size.height) afterScreenUpdates:YES];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIColor *tintColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _bg.image = [newImage applyBlurWithRadius:5 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
    
    _billHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150.f)];
    _billHeader.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    [self.view addSubview:_billHeader];
    
    _billHeaderLine = [[UIView alloc] initWithFrame:CGRectMake(0, 148.f, self.view.frame.size.width, 2)];
    _billHeaderLine.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_billHeaderLine];
    
    _categoryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 44.f, 44.f)];
    _categoryImageView.image = [UIImage imageNamed:@"money_bill_icon"];
    [self.view addSubview:_categoryImageView];
    
    _payeeLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 16, 180, 20)];
    _payeeLabel.text = @"Verizon Wireless";
    _payeeLabel.textColor = [UIColor whiteColor];
    _payeeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.f];
    [self.view addSubview:_payeeLabel];
    
    _checkButton = [[UIButton alloc] initWithFrame:CGRectMake(270, 5, 44, 44)];
    [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_cell_button"] forState:UIControlStateNormal];
    [_checkButton setBackgroundImage:[UIImage imageNamed:@"checking_cell_button" ] forState:UIControlStateHighlighted];
    [_checkButton setBackgroundImage:[UIImage imageNamed:@"checked_cell_button"] forState:UIControlStateSelected];
    [_checkButton addTarget:self action:@selector(toggleBillPaid:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_checkButton];
    
    _amountTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 100, 20)];
    _amountTextLabel.text = @"AMOUNT";
    _amountTextLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    _amountTextLabel.textAlignment = NSTextAlignmentLeft;
    _amountTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.f];
    [self.view addSubview:_amountTextLabel];
    
    _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 68, 100, 40)];
    _amountLabel.text = @"246.75";
    _amountLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    _amountLabel.textAlignment = NSTextAlignmentLeft;
    _amountLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30.f];
    [self.view addSubview:_amountLabel];
    
    _dueDateTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 60, 100, 20)];
    _dueDateTextLabel.text = @"DŪ";
    _dueDateTextLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    _dueDateTextLabel.textAlignment = NSTextAlignmentRight;
    _dueDateTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.f];
    [self.view addSubview:_dueDateTextLabel];
    
    _dueDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 68, 100, 40)];
    _dueDateLabel.text = @"DEC. 1";
    _dueDateLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    _dueDateLabel.textAlignment = NSTextAlignmentRight;
    _dueDateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30.f];
    [self.view addSubview:_dueDateLabel];
    
//    _otherBillsButton = [[UIButton alloc] initWithFrame:CGRectMake(192, 105, 44, 44)];
//    _otherBillsButton.tag = 1;
//    [_otherBillsButton setBackgroundImage:[UIImage imageNamed:@"list_bill_icon"] forState:UIControlStateNormal];
//    [_otherBillsButton addTarget:self action:@selector(showTab:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_otherBillsButton];
//    
//    _notesButton = [[UIButton alloc] initWithFrame:CGRectMake(234, 105, 44, 44)];
//    _notesButton.tag = 2;
//    _notesButton.alpha = 0.6;
//    [_notesButton setBackgroundImage:[UIImage imageNamed:@"notes_bill_icon"] forState:UIControlStateNormal];
//    [_notesButton addTarget:self action:@selector(showTab:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_notesButton];
//    
//    _settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(272, 105, 44, 44)];
//    _settingsButton.tag = 3;
//    _settingsButton.alpha = 0.6;
//    [_settingsButton setBackgroundImage:[UIImage imageNamed:@"settings_bill_icon"] forState:UIControlStateNormal];
//    [_settingsButton addTarget:self action:@selector(showTab:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_settingsButton];
    
    _billsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 150, self.view.frame.size.width, self.view.frame.size.height - 214) style:UITableViewStylePlain];
    _billsTableView.delegate = self;
    _billsTableView.dataSource = self;
    _billsTableView.backgroundColor = [UIColor clearColor];
    [_billsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_billsTableView setAllowsSelection:YES];
    [self.view addSubview:_billsTableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cell";
    Cell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[Cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.payeeLabel.text = @"Verizon Wireless";
    cell.amountLabel.text = @"141.34";
    cell.dueDateLabel.text = @"DEC. 15";
    cell.userImageView.image = [UIImage imageNamed:@"profile_image.jpg"];
    cell.userLabel.text = @"Created By Daniel Burke";
    cell.delegate = self;
    return cell;
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return 2;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    if(index == 0){
        return 40;
    }
    return 60;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    if(index == 0){
        return UIColorFromRGB(0x3399ff);
    }
    return UIColorFromRGB(0xcc9999);
}

#pragma mark BillCellDelegate methods
-(void)didToggleBillPaid:(UIButton*)button{
    if(button.isSelected){
        [button setSelected:NO];
    }
    else{
        [button setSelected:YES];
    }
    //ALSO UPDATE THE BILL MODEL AND SAVE
    [_billsTableView reloadData];
}

-(void)didTapCell:(id)sender{
}

#pragma mark Custom Methods
-(void)toggleBillPaid:(id)sender{
    
}
-(void)showTab:(id)sender{
    UIButton *button = (UIButton*)sender;
    _otherBillsButton.alpha = 0.6;
    _notesButton.alpha = 0.6;
    _settingsButton.alpha = 0.6;
    [_billsTableView setHidden:YES];
    [_notesTableView setHidden:YES];
    [_settingsTableView setHidden:YES];
    
    switch (button.tag) {
        case 1:
            _otherBillsButton.alpha = 1;
            [_billsTableView setHidden:NO];
            break;
        case 2:
            _notesButton.alpha = 1;
            [_notesTableView setHidden:NO];
            break;
        case 3:
            _settingsButton.alpha = 1;
            [_settingsTableView setHidden:NO];
            break;
            
        default:
            break;
    }
}

@end
