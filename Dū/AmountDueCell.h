//
//  AmountDueCell.h
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmountDueCell : UITableViewCell

@property (copy, nonatomic) UIView *cellView1;
@property (copy, nonatomic) UIView *cellView2;
@property (copy, nonatomic) UITextField *amountTextField;
@property (copy, nonatomic) UIButton *dueButton;
@property (copy, nonatomic) UILabel *dayLabel;
@property (copy, nonatomic) UILabel *monthLabel;
@property (copy, nonatomic) UILabel *yearLabel;

@end
