//
//  DUBillViewController.h
//  Dū
//
//  Created by Daniel Burke on 12/13/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cell.h"
#import "XYPieChart.h"

@interface DUBillViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIScrollViewDelegate, XYPieChartDelegate, XYPieChartDataSource, BillCellDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIImageView *bg;
@property (strong, nonatomic) UIView *billHeader;
@property (strong, nonatomic) UIView *billHeaderLine;
@property (strong, nonatomic) UITableView *billsTableView;
@property (strong, nonatomic) UITableView *notesTableView;
@property (strong, nonatomic) UITableView *settingsTableView;

@property (copy, nonatomic) UIImageView *categoryImageView;
@property (copy, nonatomic) UILabel *payeeLabel;
@property (copy, nonatomic) UILabel *amountLabel;
@property (copy, nonatomic) UILabel *dueDateLabel;
@property (copy, nonatomic) UILabel *amountTextLabel;
@property (copy, nonatomic) UILabel *dueDateTextLabel;
@property (copy, nonatomic) UILabel *otherBillsTextLabel;
@property (copy, nonatomic) UIButton *checkButton;
@property (copy, nonatomic) UIButton *otherBillsButton;
@property (copy, nonatomic) UIButton *settingsButton;
@property (copy, nonatomic) UIButton *notesButton;
@property (strong, nonatomic) UITextView *notesTextView;
@property (copy, nonatomic) UIImageView *isRecurringIcon;

@property (nonatomic) BOOL isLate;
@property (nonatomic) BOOL isPaid;
@property (nonatomic) BOOL isRecurring;

-(void)toggleBillPaid:(id)sender;
-(void)showTab:(id)sender;
@end
