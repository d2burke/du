//
//  DUAppDelegate.h
//  Dū
//
//  Created by Daniel Burke on 12/13/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Payee.h"
#import "Reminder.h"
#import "User.h"
#import "Note.h"
#import "Bill.h"
#import "RBill.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define randomUIColor() return [UIColor colorWithRed:arc4random() % 255/255 green: arc4random() % 255/255 blue:arc4random() % 255/255 alpha:1.0f]

@class DUBillsListViewController;

@interface DUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DUBillsListViewController *listViewController;
@property (strong, nonatomic) UIImageView *bg;
@property (strong, nonatomic) UINavigationController *navController;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
