//
//  PayeeCell.m
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "PayeeCell.h"

@implementation PayeeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGRect cellFrame = self.frame;
        cellFrame.size.height = 44;
        _cellView = [[UIView alloc] initWithFrame:cellFrame];
        _cellView.backgroundColor = [UIColor whiteColor];
        _payeeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        _payeeIcon.image = [UIImage imageNamed:@"user_nav_button"];
        _payeeIcon.tag = 1;
        _payeeTextField = [[UITextField alloc] initWithFrame:CGRectMake(44, 0, 220, 44)];
        _payeeTextField.tag = 2;
        _payeeTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _payeeTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _payeeTextField.placeholder = @"Payee";
        _payeeTextField.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _payeeTextField.returnKeyType = UIReturnKeyDone;
        _payeeTextField.textColor = [UIColor grayColor];
        
        _createNewLabel = [[UILabel alloc] initWithFrame:CGRectMake(264, 12, 44, 22)];
        _createNewLabel.tag = 5;
        _createNewLabel.text = @"NEW";
        _createNewLabel.textColor = [UIColor whiteColor];
        _createNewLabel.backgroundColor = [UIColor grayColor];
        _createNewLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        _createNewLabel.textAlignment = NSTextAlignmentCenter;
        _createNewLabel.layer.cornerRadius = 5.f;
        [_createNewLabel setHidden:YES];
        
        [self addSubview:_cellView];
        [self addSubview:_payeeIcon];
        [self addSubview:_payeeTextField];
        [self addSubview:_createNewLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
