//
//  DUCalendarPickerViewController.m
//  Dū
//
//  Created by Daniel Burke on 12/16/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "DUCalendarPickerViewController.h"

@interface DUCalendarPickerViewController ()

@end

@implementation DUCalendarPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initNav];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initNav{
    self.navigationItem.hidesBackButton = YES;
    
    UIView *navContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIImage *navLogoImage = [UIImage imageNamed:@"logo_nav_button"];
    //    UIImage *navLogoImageDown = [UIImage imageNamed:@"logo_nav_button"];
    UIButton *navLogo = [[UIButton alloc] initWithFrame:CGRectMake(132, 0, 44, 44)];
    navLogo.tintColor = [UIColor grayColor];
    [navLogo setBackgroundImage:navLogoImage forState:UIControlStateNormal];
    //    [navLogo setBackgroundImage:navLogoImageDown forState:UIControlStateHighlighted];
    //    [navLogo addTarget:self action:@selector(showPopOver:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:navLogo];
    
    UIImage *backImage = [UIImage imageNamed:@"back_nav_button"];
    //    UIImage *backImageDown = [UIImage imageNamed:@"down_nav_button"];
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0-5, 0, 44, 44)];
    backBtn.tintColor = [UIColor grayColor];
    [backBtn setBackgroundImage:backImage forState:UIControlStateNormal];
    //    [backBtn setBackgroundImage:backImageDown forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:backBtn];
    
    UIImage *createImage = [UIImage imageNamed:@"check_nav_button"];
    //    UIImage *createImageDown = [UIImage imageNamed:@"check_nav_button"];
    UIButton *createBtn = [[UIButton alloc] initWithFrame:CGRectMake(265, 0, 44, 44)];
    createBtn.tintColor = [UIColor grayColor];
    [createBtn setBackgroundImage:createImage forState:UIControlStateNormal];
    //    [reuseBtn setBackgroundImage:reuseImageDown forState:UIControlStateHighlighted];
    [createBtn addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:createBtn];
    
    [self.navigationItem setTitleView:navContainer];
}

-(void)cancel:(id)sender{
    [_delegate didCancelSelectDate:self];
}

-(void)selectDate:(id)sender{
    [_delegate didSelectDate:[NSDate date]];
}

@end
