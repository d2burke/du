//
//  DUCalendarPickerViewController.h
//  Dū
//
//  Created by Daniel Burke on 12/16/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarPickerDelegate <NSObject>

-(void)didSelectDate:(NSDate*)date;
-(void)didCancelSelectDate:(id)sender;

@end

@interface DUCalendarPickerViewController : UIViewController

@property (strong, nonatomic) id<CalendarPickerDelegate> delegate;
@property (strong, nonatomic) UICollectionView *calendarPicker;

-(void)selectDate:(id)sender;
-(void)cancel:(id)sender;

@end
