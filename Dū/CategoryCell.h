//
//  CategoryCell.h
//  Dū
//
//  Created by Daniel Burke on 12/23/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UITableViewCell

@property (strong, nonatomic) UIImageView *icon;
@property (strong, nonatomic) UILabel *label;

@end
