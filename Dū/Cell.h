//
//  Cell.h
//  CustomTableCell
//
//  Created by Daniel Burke on 12/5/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@protocol BillCellDelegate <NSObject>

-(void)didToggleBillPaid:(UIButton*)button;
-(void)didTapCell:(id)sender;
-(void)didDeleteCell:(NSIndexPath*)path;

@end

@interface Cell : UITableViewCell <UIScrollViewDelegate>

@property (strong, nonatomic) id<BillCellDelegate> delegate;
@property (weak, nonatomic) UITableView *parentTableView;
@property (copy, nonatomic) UITapGestureRecognizer *tapGesture;
@property (copy, nonatomic) UIScrollView *topScrollView;
@property (copy, nonatomic) UIView *topView;
@property (copy, nonatomic) UIView *deleteView;
@property (copy, nonatomic) UILabel *deleteLabel;
@property (copy, nonatomic) UILabel *deleteSubtitleLabel;
@property (copy, nonatomic) UIImageView *deleteImageView;
@property (copy, nonatomic) UIImage *deleteIcon;
@property (copy, nonatomic) UIImage *trashIcon;
@property (copy, nonatomic) UIView *markPaidView;
@property (copy, nonatomic) UILabel *markPaidLabel;
@property (copy, nonatomic) UIImageView *userImageView;
@property (copy, nonatomic) UIImageView *categoryImageView;
@property (copy, nonatomic) UILabel *payeeLabel;
@property (copy, nonatomic) UILabel *amountLabel;
@property (copy, nonatomic) UILabel *dueDateLabel;
@property (copy, nonatomic) UIButton *checkButton;

@property (copy, nonatomic) UIView *botView;
@property (copy, nonatomic) UILabel *userLabel;
@property (copy, nonatomic) UIImageView *isLateIcon;
@property (copy, nonatomic) UIImageView *hasNotesIcon;
@property (copy, nonatomic) UIImageView *isRecurringIcon;

@property (nonatomic) BOOL isLate;
@property (nonatomic) BOOL isPaid;
@property (nonatomic) BOOL isRecurring;

@property (nonatomic) CGRect cellFrame;
@property (strong, nonatomic) CALayer *cellMask;

@end
