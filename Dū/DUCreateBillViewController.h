//
//  DUCreateBillViewController.h
//  Dū
//
//  Created by Daniel Burke on 12/14/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DUAppDelegate.h"
#import "CategoryCell.h"
#import "PayeeCell.h"
#import "AmountDueCell.h"
#import "PaidRecurringCell.h"
#import "ReminderCell.h"
#import "NotesCell.h"
#import "ReminderTypeCell.h"
#import "DUCalendarPickerViewController.h"

@protocol CreateBillViewControllerDelegate <NSObject>

-(void)didCancelCreateBill:(id)sender;
-(void)didCreateBill:(id)sender withBill:(Bill*)bill;

@end

typedef enum{
    kRecurringTypeOnce,
    kRecurringTypeDaily,
    kRecurringTypeBiMonthly,
    kRecurringTypeMonthly,
    kRecurringTypeAnnually
}RecurringType;

@interface DUCreateBillViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIPickerViewDelegate, ReminderCellDelegate>

@property (strong, nonatomic) DUAppDelegate *appDelegate;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSError *error;
@property (strong, nonatomic) id<CreateBillViewControllerDelegate> delegate;
@property (nonatomic) RecurringType recurringType;
@property (strong, nonatomic) UIView *snapView;
@property (strong, nonatomic) NSArray *recurringTypes;
@property (strong, nonatomic) UILabel *recurringTypeLabel;
@property (strong, nonatomic) UILabel *recurringTypeTextLabel;
@property (strong, nonatomic) UIImageView *bg;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *payeeView;
@property (strong, nonatomic) UITableView *payeeTableView;
@property (strong, nonatomic) UIView *categoryView;
@property (strong, nonatomic) UITableView *categoryTableView;
@property (strong, nonatomic) UIView *reminderView;
@property (strong, nonatomic) UITableView *reminderTableView;
@property (copy, nonatomic) NSArray *reminderTypes;
@property (strong, nonatomic) UIView *iconView;
@property (strong, nonatomic) UICollectionView *iconCollectionView;
@property (strong, nonatomic) NSString *payeeName;
@property (strong, nonatomic) Payee *chosen_payee;
@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) NSString *categoryIcon;
@property (nonatomic) BOOL createNewCategory;
@property (nonatomic) BOOL createNewPayee;
@property (strong, nonatomic) UIView *recurringView;
@property (strong, nonatomic) UITableView *recurringTableView;
@property (strong, nonatomic) UITextField *currentTextField;
@property (strong, nonatomic) NSArray *iconCatalog;
@property (strong, nonatomic) NSArray *categoryCatalog;
@property (strong, nonatomic) NSMutableArray *categorySearchMatches;

@property (copy, nonatomic) UITextField *amountTextField;
@property (copy, nonatomic) UITextView *notesTextView;
@property (copy, nonatomic) UIButton *recurringButton;
@property (copy, nonatomic) UIButton *checkButton;
@property (copy, nonatomic) UILabel *paidLabel;

@property (nonatomic) BOOL datePickerIsShowing;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *dateChosen;
@property (strong, nonatomic) UIView *pickerView;
@property (strong, nonatomic) UIButton *chooseButton;
@property (strong, nonatomic) UIButton *cancelButton;
@property (copy, nonatomic) UILabel *dayLabel;
@property (copy, nonatomic) UILabel *monthLabel;
@property (copy, nonatomic) UILabel *yearLabel;
@property (copy, nonatomic) UIButton *dueButton;
@property (copy, nonatomic) UIButton *reminderButton;

@property (nonatomic) BOOL reminderPickerIsShowing;
@property (strong, nonatomic) UIDatePicker *timePicker;

@property (strong, nonatomic) NSMutableArray *payees;
@property (strong, nonatomic) NSMutableArray *payeeCatalog;
@property (strong, nonatomic) NSMutableArray *payeeSearchMatches;
@property (strong, nonatomic) NSMutableArray *reminders;

- (IBAction)dateChanged:(UIDatePicker *)sender;
-(void)cancelCreate:(id)sender;

@end
