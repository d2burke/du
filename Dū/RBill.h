//
//  RBill.h
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RBill : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * du;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * paid;
@property (nonatomic, retain) NSString * period;
@property (nonatomic, retain) NSDate * synced;
@property (nonatomic, retain) NSManagedObject *notes;
@property (nonatomic, retain) NSManagedObject *payee;
@property (nonatomic, retain) NSManagedObject *reminders;
@property (nonatomic, retain) NSManagedObject *user;

@end
