//
//  Bill.m
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "Bill.h"


@implementation Bill

@dynamic name;
@dynamic category;
@dynamic amount;
@dynamic du;
@dynamic paid;
@dynamic created;
@dynamic synced;
@dynamic user;
@dynamic notes;
@dynamic reminders;
@dynamic payee;

@end
