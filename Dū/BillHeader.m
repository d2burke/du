//
//  BillHeader.m
//  CustomTableCell
//
//  Created by Daniel Burke on 12/8/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "BillHeader.h"
#import "UIImage+ImageEffects.h"

@implementation BillHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.frame = CGRectMake(0, 0, 320, 120.f);

        _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 115)];
        _backgroundImageView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        _backgroundImageView.contentMode = UIViewContentModeTopLeft;
        [self addSubview:_backgroundImageView];
        
        _monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 17, 50, 30)];
        _monthLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
        _monthLabel.textAlignment = NSTextAlignmentCenter;
        _monthLabel.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
        _monthLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.f];
        [self addSubview:_monthLabel];
        
        _totalAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, 100, 20)];
        _totalAmountLabel.textColor = [UIColor whiteColor];
        _totalAmountLabel.textAlignment = NSTextAlignmentRight;
        _totalAmountLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.f];
        [self addSubview:_totalAmountLabel];
        
        _totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 20, 50, 20)];
        _totalLabel.textColor = [UIColor whiteColor];
        _totalLabel.textAlignment = NSTextAlignmentLeft;
        _totalLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.f];
        _totalLabel.text = @"TOTAL";
        [self addSubview:_totalLabel];
        
        _paidAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 40, 100, 20)];
        _paidAmountLabel.textColor = [UIColor whiteColor];
        _paidAmountLabel.textAlignment = NSTextAlignmentRight;
        _paidAmountLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.f];
        [self addSubview:_paidAmountLabel];
        
        _paidLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 40, 100, 20)];
        _paidLabel.textColor = [UIColor whiteColor];
        _paidLabel.textAlignment = NSTextAlignmentLeft;
        _paidLabel.text = @"PAID";
        _paidLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.f];
        [self addSubview:_paidLabel];
        
        _dueAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 60, 100, 20)];
        _dueAmountLabel.textColor = [UIColor whiteColor];
        _dueAmountLabel.textAlignment = NSTextAlignmentRight;
        _dueAmountLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.f];
        [self addSubview:_dueAmountLabel];
        
        _dueLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 60, 100, 20)];
        _dueLabel.textColor = [UIColor whiteColor];
        _dueLabel.textAlignment = NSTextAlignmentLeft;
        _dueLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20.f];
        _dueLabel.text = @"DŪ";
        [self addSubview:_dueLabel];
        
        _lateAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 80, 100, 20)];
        _lateAmountLabel.textColor = [UIColor whiteColor];
        _lateAmountLabel.textAlignment = NSTextAlignmentRight;
        _lateAmountLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.f];
        [self addSubview:_lateAmountLabel];
        
        _lateLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 80, 100, 20)];
        _lateLabel.textColor = [UIColor whiteColor];
        _lateLabel.textAlignment = NSTextAlignmentLeft;
        _lateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.f];
        _lateLabel.text = @"LATE";
        [self addSubview:_lateLabel];
        
        UIView *pieBgView = [[UIView alloc] initWithFrame:CGRectMake(217.5f, 12.5, 95, 95)];
        pieBgView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.2];
        pieBgView.layer.cornerRadius = CGRectGetHeight(pieBgView.bounds) / 2;
        [self addSubview:pieBgView];
        
        _percentPaidLabel = [[UILabel alloc] initWithFrame:CGRectMake(217.5f, 50, 95, 35)];
        _percentPaidLabel.text = @"PAID";
        _percentPaidLabel.textAlignment = NSTextAlignmentCenter;
        _percentPaidLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
        _percentPaidLabel.textColor = [UIColor whiteColor];
        [self addSubview:_percentPaidLabel];
        
        _percentLabel = [[UILabel alloc] initWithFrame:CGRectMake(217.5f, 33, 95, 35)];
        _percentLabel.text = @"50%";
        _percentLabel.textAlignment = NSTextAlignmentCenter;
        _percentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20.f];
        _percentLabel.textColor = [UIColor whiteColor];
        [self addSubview:_percentLabel];
        
//        _pieChart = [[XYPieChart alloc] initWithFrame:CGRectMake(215, 10, 100, 100)];
//        [self addSubview:_pieChart];
//        _pieChart.labelFont = [UIFont fontWithName:@"HelveticaNeue" size:12.f];
//        [_pieChart setPieBackgroundColor:[UIColor clearColor]];
//        [_pieChart setPieCenter:CGPointMake(50, 50)];
//        [_pieChart setLabelRadius:30];
//        [_pieChart reloadData];
        
//        UIView *donut = [[UIView alloc] initWithFrame:CGRectMake(245, 40, 40, 40)];
//        donut.backgroundColor = [UIColor whiteColor];
//        donut.layer.cornerRadius = CGRectGetHeight(donut.bounds) / 2;
//        [self addSubview:donut];

        _pieChart = [[PieChartView alloc] initWithFrame:CGRectMake(215, 10, 100, 100)];
        [self addSubview:_pieChart];

    }
    return self;
}


- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart{
    return 2;
}
- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index{
    return 50;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
