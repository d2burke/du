//
//  RBill.m
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "RBill.h"


@implementation RBill

@dynamic amount;
@dynamic category;
@dynamic created;
@dynamic du;
@dynamic name;
@dynamic paid;
@dynamic period;
@dynamic synced;
@dynamic notes;
@dynamic payee;
@dynamic reminders;
@dynamic user;

@end
