//
//  NotesCell.h
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesCell : UITableViewCell

@property (copy, nonatomic) UITextView *notesTextView;

@end
