//
//  ReminderTypeCell.h
//  Dū
//
//  Created by Daniel Burke on 12/25/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReminderTypeCell : UITableViewCell

@property (strong, nonatomic) UILabel *label;

@end
