//
//  DUBillsListViewController.h
//  Dū
//
//  Created by Daniel Burke on 12/13/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DUAppDelegate.h"
#import "Cell.h"
#import "CollectionCell.h"
#import "BillHeader.h"
#import "XYPieChart.h"
#import "PieChartView.h"
#import "DUCreateBillViewController.h"

@interface DUBillsListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIScrollViewDelegate, UIScrollViewDelegate, XYPieChartDelegate, XYPieChartDataSource, PieChartViewDelegate, PieChartViewDataSource, BillCellDelegate, CreateBillViewControllerDelegate>

@property (strong, nonatomic) DUAppDelegate *appDelegate;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) UIView *searchPanelView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIToolbar *toolbar;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIView *snap;
@property (strong, nonatomic) UIImageView *bg;

@property (nonatomic) CGRect buttonFrame;
@property (strong, nonatomic) CALayer *maskLayer;

@property (strong, nonatomic) NSMutableArray *monthKeys;
@property (strong, nonatomic) NSMutableDictionary *months;
@property (copy, nonatomic) NSMutableDictionary *month;
@property (strong, nonatomic) NSMutableArray *bills;
@property (copy, nonatomic) NSMutableDictionary *bill;
@property (copy, nonatomic) NSMutableArray *headerChartValues;

@property (nonatomic) int numberOfCells;

-(void)createBill:(id)sender;

@end
