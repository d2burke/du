//
//  Payee.m
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "Payee.h"
#import "Bill.h"
#import "RBill.h"


@implementation Payee

@dynamic name;
@dynamic category;
@dynamic phone;
@dynamic web;
@dynamic image;
@dynamic synced;
@dynamic bills;
@dynamic rbills;

@end
