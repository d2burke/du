//
//  PaidRecurringCell.h
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaidRecurringCell : UITableViewCell

@property (copy, nonatomic) UIView *cellView1;
@property (copy, nonatomic) UIView *cellView2;
@property (copy, nonatomic) UIButton *checkButton;
@property (copy, nonatomic) UIButton *recurringButton;
@property (copy, nonatomic) UILabel *paidLabel;
@property (copy, nonatomic) UILabel *recurringTypeTextLabel;
@property (copy, nonatomic) UILabel *recurringTypeLabel;

@end
