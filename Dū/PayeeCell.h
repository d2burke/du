//
//  PayeeCell.h
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayeeCell : UITableViewCell

@property (copy, nonatomic) UITextField *payeeTextField;
@property (copy, nonatomic) UIImageView *payeeIcon;
@property (copy, nonatomic) UILabel *createNewLabel;
@property (copy, nonatomic) UIView *cellView;

@end
