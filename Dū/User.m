//
//  User.m
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "User.h"
#import "Bill.h"


@implementation User

@dynamic firstname;
@dynamic lastname;
@dynamic username;
@dynamic email;
@dynamic pin;
@dynamic password;
@dynamic image;
@dynamic facebookid;
@dynamic twitterid;
@dynamic synced;
@dynamic created;
@dynamic bills;

@end
