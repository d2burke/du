//
//  IconCollectionCell.h
//  Dū
//
//  Created by Daniel Burke on 12/15/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconCollectionCell : UICollectionViewCell

@property (copy, nonatomic) UIImageView *icon;
@property (copy, nonatomic) NSString *name;

@end
