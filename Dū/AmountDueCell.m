//
//  AmountDueCell.m
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "AmountDueCell.h"

@implementation AmountDueCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        _cellView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 159, 44)];
        _cellView1.backgroundColor = [UIColor whiteColor];
        
        UIImageView *amountIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        amountIcon.image = [UIImage imageNamed:@"money_cell_icon"];
        _amountTextField = [[UITextField alloc] initWithFrame:CGRectMake(44, 0, 220, 44)];
        _amountTextField.placeholder = @"Amount";
        _amountTextField.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _amountTextField.textColor = [UIColor grayColor];
        _amountTextField.keyboardType = UIKeyboardTypeNumberPad;
        _amountTextField.returnKeyType = UIReturnKeyNext;
        [_cellView1 addSubview:amountIcon];
        [_cellView1 addSubview:_amountTextField];
        
        _cellView2 = [[UIView alloc] initWithFrame:CGRectMake(161, 0, 159, 44)];
        _cellView2.backgroundColor = [UIColor whiteColor];
        _dueButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _dueButton.frame = CGRectMake(0, 2, 159, 44);
        [_dueButton setImage:[UIImage imageNamed:@"calendar_cell_icon"] forState:UIControlStateNormal];
        [_dueButton setImageEdgeInsets:UIEdgeInsetsMake(-2, 0, 0, 114)];
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(44, 0, 220, 44)];
        _dayLabel.tag = 1;
        _dayLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
        _dayLabel.textColor = [UIColor grayColor];
        
        _monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 10, 100, 15)];
        _monthLabel.tag = 2;
        _monthLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:9];
        _monthLabel.textColor = [UIColor grayColor];
        
        _yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 20, 100, 15)];
        _yearLabel.tag = 3;
        _yearLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:9];
        _yearLabel.textColor = [UIColor grayColor];
        
        NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
        [dayFormatter setDateFormat:@"dd"];
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        [monthFormatter setDateFormat:@"MMM"];
        NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
        [yearFormatter setDateFormat:@"Y"];
        
        _dayLabel.text = [dayFormatter stringFromDate:[NSDate date]];
        _monthLabel.text = [monthFormatter stringFromDate:[NSDate date]];
        _yearLabel.text = [yearFormatter stringFromDate:[NSDate date]];
        
        [_cellView2 addSubview:_dueButton];
        [_cellView2 addSubview:_dayLabel];
        [_cellView2 addSubview:_monthLabel];
        [_cellView2 addSubview:_yearLabel];
        
        [self addSubview:_cellView1];
        [self addSubview:_cellView2];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
