//
//  ReminderTypeCell.m
//  Dū
//
//  Created by Daniel Burke on 12/25/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "ReminderTypeCell.h"

@implementation ReminderTypeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        _label = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 277, 40)];
        _label.tag = 1;
        _label.textColor = [UIColor grayColor];
        _label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        [self addSubview:_label];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
