//
//  Cell.m
//  CustomTableCell
//
//  Created by Daniel Burke on 12/5/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "Cell.h"

@implementation Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _isPaid = NO;
        _isLate = NO;
        _isRecurring = NO;
        
        _topScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 89.f)];
        _topScrollView.contentSize = CGSizeMake(321, 49.f);
        _topScrollView.bounces = YES;
        _topScrollView.showsHorizontalScrollIndicator = NO;
        _topScrollView.scrollsToTop = NO;
        _topScrollView.delegate = self;
        
        _topView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 321, 59.f)];
        _topView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
        
        _deleteView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 321, 59.f)];
        _deleteView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
        _deleteView.alpha = 0;
        
        _deleteSubtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.f, 10.f, 200.f, 20)];
        _deleteSubtitleLabel.textColor = [UIColor whiteColor];
        _deleteSubtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.f];
        _deleteSubtitleLabel.text = @"SLIDE TO";
        
        _deleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.f, 23, 200.f, 30)];
        _deleteLabel.textColor = [UIColor whiteColor];
        _deleteLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.f];
        _deleteLabel.text = @"DELETE BILL";
        
        _deleteIcon = [UIImage imageNamed:@"delete_cell_icon"];
        _trashIcon = [UIImage imageNamed:@"trash_cell_icon"];
        _deleteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-44, 9, 44, 44)];
        _deleteImageView.image = [UIImage imageNamed:@"trash_cell_icon"];
        _deleteImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        _markPaidView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 321, 59.f)];
        _markPaidView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
        _markPaidView.alpha = 0;
        
        _categoryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 9, 44, 44)];
        _categoryImageView.image = [UIImage imageNamed:@"money_cell_icon"];
        _categoryImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        _checkButton = [[UIButton alloc] initWithFrame:CGRectMake(270, 9, 44, 44)];
        [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_cell_button"] forState:UIControlStateNormal];
        [_checkButton setBackgroundImage:[UIImage imageNamed:@"checking_cell_button" ] forState:UIControlStateHighlighted];
        [_checkButton setBackgroundImage:[UIImage imageNamed:@"checked_cell_button"] forState:UIControlStateSelected];
        [_checkButton addTarget:self action:@selector(toggleBillPaid:) forControlEvents:UIControlEventTouchUpInside];
        
        _payeeLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.f, 10.f, 100.f, 20)];
        _payeeLabel.textColor = [UIColor grayColor];
        _payeeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.f];
        
        _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.f, 23, 100.f, 30)];
        _amountLabel.textColor = [UIColor grayColor];
        _amountLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.f];
        
        _dueDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(160.f, 23, 100.f, 30)];
        _dueDateLabel.textColor = [UIColor grayColor];
        _dueDateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.f];
        _dueDateLabel.textAlignment = NSTextAlignmentRight;
        
        _botView = [[UIView alloc] initWithFrame:CGRectMake(0, 60.f, self.frame.size.width, 29.f)];
        _botView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        
        _userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 25, 25)];
        _userImageView.backgroundColor = [UIColor grayColor];
        _userImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        _userLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 4, 150, 20)];
        _userLabel.textColor = [UIColor whiteColor];
        _userLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.f];
        
        [_deleteView addSubview:_deleteSubtitleLabel];
        [_deleteView addSubview:_deleteLabel];
        [_topScrollView addSubview:_deleteView];
        [_topScrollView addSubview:_deleteImageView];
        [_topScrollView addSubview:_markPaidView];
        
        [_topView addSubview:_categoryImageView];
        [_topView addSubview:_payeeLabel];
        [_topView addSubview:_amountLabel];
        [_topView addSubview:_dueDateLabel];
        [_topView addSubview:_checkButton];
        [_topScrollView addSubview:_topView];
        [_botView addSubview:_userLabel];
        [_botView addSubview:_userImageView];
        [self addSubview:_botView];
        [self addSubview:_topScrollView];
        
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
        _tapGesture.delegate = self;
        [self addGestureRecognizer:_tapGesture];
        
        [self setUserInteractionEnabled:YES];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)layoutSubviews{
    
    if (_isLate) {
        _botView.backgroundColor = UIColorFromRGB(0xcc9999);
        _dueDateLabel.textColor = UIColorFromRGB(0xcc9999);
    }
    
    if(_isPaid){
        [_checkButton setSelected:YES];
    }
    else{
        [_checkButton setSelected:NO];
        _botView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        _dueDateLabel.textColor = UIColorFromRGB(0x999999);
    }
}

#pragma mark UIScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == _topScrollView) {
        if(scrollView.contentOffset.x < 0){
            if (scrollView.contentOffset.x < -44) {
                CGRect deleteFrame = _deleteImageView.frame;
                deleteFrame.origin.x = -22 + (scrollView.contentOffset.x)/2;
                _deleteImageView.frame = deleteFrame;
                if(scrollView.contentOffset.x < -80){
                    _deleteSubtitleLabel.text = @"RELEASE TO";
                    _deleteImageView.image = _deleteIcon;
                }
                else if (scrollView.contentOffset.x > -80){
                    _deleteSubtitleLabel.text = @"SLIDE TO";
                    _deleteImageView.image = _trashIcon;
                }
            }
            _topView.alpha = 1 + scrollView.contentOffset.x/40;
            _deleteView.alpha = -1*(scrollView.contentOffset.x/40);
        }
        else if(scrollView.contentOffset.x > 0){
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView == _topScrollView) {
        _topView.alpha = 1;
        _deleteView.alpha = 0;
        _markPaidView.alpha = 0;
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    NSLog(@"Offset: %0.2f", scrollView.contentOffset.x);
    if(scrollView.contentOffset.x <= -80){
        NSLog(@"Delete Bill");
        NSIndexPath *indexPath = [(UITableView*)[self.superview superview] indexPathForCell:self];
        [_delegate didDeleteCell:indexPath];
    }
}

-(void)toggleBillPaid:(UIButton*)button{
    [_delegate didToggleBillPaid:button];
}

-(void)tappedCell:(id)sender{
    [_delegate didTapCell:sender];
}

@end
