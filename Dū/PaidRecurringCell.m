//
//  PaidRecurringCell.m
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "PaidRecurringCell.h"

@implementation PaidRecurringCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        _cellView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 159, 44)];
        _cellView1.backgroundColor = [UIColor whiteColor];
        _checkButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 159, 44)];
        [_checkButton setContentMode:UIViewContentModeLeft];
        [_checkButton setImage:[UIImage imageNamed:@"check_cell_button"] forState:UIControlStateNormal];
        [_checkButton setImage:[UIImage imageNamed:@"checking_cell_button" ] forState:UIControlStateHighlighted];
        [_checkButton setImage:[UIImage imageNamed:@"checked_cell_button"] forState:UIControlStateSelected];
        [_checkButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 114)];
        [_checkButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -30)];
        
        _paidLabel = [[UILabel alloc] initWithFrame:CGRectMake(44, 0, 120, 44)];
        _paidLabel.text = @"Unpaid";
        _paidLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _paidLabel.textColor = [UIColor grayColor];
        
        _cellView2 = [[UIView alloc] initWithFrame:CGRectMake(161, 0, 159, 44)];
        _cellView2.backgroundColor = [UIColor whiteColor];
        
        _recurringTypeTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(44, 5, 120, 15)];
        _recurringTypeTextLabel.text = @"Happens";
        _recurringTypeTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
        _recurringTypeTextLabel.textColor = [UIColor grayColor];
        
        _recurringTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(44, 12, 120, 29)];
        _recurringTypeLabel.text = @"Once";
        _recurringTypeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _recurringTypeLabel.textColor = [UIColor grayColor];
        
        _recurringButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _recurringButton.frame = CGRectMake(0, 0, 159, 44);
        [_recurringButton setImage:[UIImage imageNamed:@"recur_cell_icon"] forState:UIControlStateNormal];
        [_recurringButton setImage:[UIImage imageNamed:@"set_recurs_cell_icon"] forState:UIControlStateHighlighted];
        [_recurringButton setImage:[UIImage imageNamed:@"recurring_cell_icon"] forState:UIControlStateSelected];
        [_recurringButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 114)];
        [_cellView1 addSubview:_checkButton];
        [_cellView1 addSubview:_paidLabel];
        [_cellView2 addSubview:_recurringTypeTextLabel];
        [_cellView2 addSubview:_recurringTypeLabel];
        [_cellView2 addSubview:_recurringButton];
        
        [self addSubview:_cellView1];
        [self addSubview:_cellView2];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
