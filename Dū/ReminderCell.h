//
//  ReminderCell.h
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReminderCellDelegate <NSObject>

-(void)didDeleteReminder:(NSIndexPath*)path;

@end

@interface ReminderCell : UITableViewCell <UIScrollViewDelegate>

typedef enum{
    kReminderTypeOnDueDate,
    kReminderTypeDayBefore,
    kReminderTypeTwoDaysBefore,
    kReminderTypeWeekBefore,
    kReminderTypeTwoWeeksBefore,
    kReminderTypeMonthBefore
}ReminderType;

@property (strong, nonatomic) id<ReminderCellDelegate> delegate;
@property (nonatomic) ReminderType reminderType;
@property (copy, nonatomic) NSArray *reminderTypes;
@property (nonatomic) BOOL isAReminder;
@property (copy, nonatomic) UIScrollView *scrollView;
@property (copy, nonatomic) UIView *cellView;
@property (copy, nonatomic) UIView *deleteView;
@property (copy, nonatomic) UILabel *deleteLabel;
@property (copy, nonatomic) UIButton *reminderButton;
@property (copy, nonatomic) UIImageView *reminderIcon;
@property (copy, nonatomic) UILabel *reminderLabel;

@end
