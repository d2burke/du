//
//  Payee.h
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bill, RBill;

@interface Payee : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * web;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSDate * synced;
@property (nonatomic, retain) Bill *bills;
@property (nonatomic, retain) RBill *rbills;

@end
