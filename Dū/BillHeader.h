//
//  BillHeader.h
//  CustomTableCell
//
//  Created by Daniel Burke on 12/8/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"
#import "PieChartView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface BillHeader : UIView

@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIView *maskView;
@property (nonatomic) CGRect cellFrame;
@property (strong, nonatomic) CALayer *cellMask;
@property (copy, nonatomic) UILabel *monthLabel;
@property (copy, nonatomic) UILabel *totalAmountLabel;
@property (copy, nonatomic) UILabel *totalLabel;
@property (copy, nonatomic) UILabel *paidAmountLabel;
@property (copy, nonatomic) UILabel *paidLabel;
@property (copy, nonatomic) UILabel *dueAmountLabel;
@property (copy, nonatomic) UILabel *dueLabel;
@property (copy, nonatomic) UILabel *lateAmountLabel;
@property (copy, nonatomic) UILabel *lateLabel;
@property (copy, nonatomic) UILabel *percentPaidLabel;
@property (copy, nonatomic) UILabel *percentLabel;
@property (nonatomic) CGFloat percentPaid;

@property (copy, nonatomic) UILabel *todayLabel;
@property (copy, nonatomic) UILabel *todayDateLabel;

//@property (copy, nonatomic) XYPieChart *pieChart;
@property (copy, nonatomic) PieChartView *pieChart;

@end
