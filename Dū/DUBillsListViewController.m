//
//  DUBillsListViewController.m
//  Dū
//
//  Created by Daniel Burke on 12/13/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "DUBillsListViewController.h"
#import "DUAppDelegate.h"
#import "UIImage+ImageEffects.h"
#import "DUBillViewController.h"
#import "DUCreateBillViewController.h"

static inline UIColor *GetRandomUIColor()
{
    CGFloat r = arc4random() % 255;
    CGFloat g = arc4random() % 255;
    CGFloat b = arc4random() % 255;
    UIColor * color = [UIColor colorWithRed:r/255 green:g/255 blue:b/255 alpha:1.0f];
    return color;
}

@interface DUBillsListViewController ()

@end

@implementation DUBillsListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.barTintColor = [[UIColor whiteColor] colorWithAlphaComponent:1];  // UIColorFromRGB(0xb25538);
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x999999);
    self.navigationController.navigationBar.translucent = NO;
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    
    [navigationBar setBackgroundImage:[UIImage new]
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    
    [navigationBar setShadowImage:[UIImage new]];
    navigationBar.backgroundColor = [UIColor whiteColor];
}

-(void)viewDidAppear:(BOOL)animated{
    [self loadBills:[NSDate date]];
    [_tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //GET GLOBAL SINGLETON CONTEXT
    _appDelegate = (DUAppDelegate *)[[UIApplication sharedApplication] delegate];
    _context = [_appDelegate managedObjectContext];
    
    _bills = [[NSMutableArray alloc] init];
    _numberOfCells = 8;
    _monthKeys = [[NSMutableArray alloc] initWithObjects:@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sept", @"Oct", @"Nov", @"Dec", nil];
    
    _headerChartValues = [@[@0,@0] mutableCopy];
    
    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView == _tableView){
        if((-1*scrollView.contentOffset.y) >= 0){
            CGRect searchFrame = _searchPanelView.frame;
            searchFrame.size.height = (-1*scrollView.contentOffset.y);
            _searchPanelView.frame = searchFrame;
        }
    }
}

#pragma mark Custom Methods

-(void)initUI{
    //CUSTOM NAV BAR
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.jpg"] forBarMetrics:UIBarMetricsDefault];
    }
    
    //CONFIGURE BACKGROUND
    _bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chalk_blur"]];
    _bg.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    _bg.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_bg];
    
    UIGraphicsBeginImageContextWithOptions(_bg.frame.size, NO, 0);
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, _bg.frame.size.width, _bg.frame.size.height) afterScreenUpdates:YES];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIColor *tintColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _bg.image = [newImage applyBlurWithRadius:5 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
    
    _searchPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    _searchPanelView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [self.view addSubview:_searchPanelView];
    
    //ADD NAV BUTTONS
    UIView *navContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIImage *navLogoImage = [UIImage imageNamed:@"logo_nav_button"];
    UIImage *navLogoImageDown = [UIImage imageNamed:@"logo_nav_button"];
    UIButton *navLogo = [[UIButton alloc] initWithFrame:CGRectMake(132, 0, 44, 44)];
    navLogo.tintColor = [UIColor grayColor];
    [navLogo setBackgroundImage:navLogoImage forState:UIControlStateNormal];
    [navLogo setBackgroundImage:navLogoImageDown forState:UIControlStateHighlighted];
    //    [navLogo addTarget:self action:@selector(showPopOver:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:navLogo];
    
    UIImage *profileImage = [UIImage imageNamed:@"profile_image.jpg"];
    UIImage *userImageDown = [UIImage imageNamed:@"user_nav_button"];
    UIButton *userBtn = [[UIButton alloc] initWithFrame:CGRectMake(-2, 2, 36, 36)];
    userBtn.tintColor = [UIColor grayColor];
    [userBtn setBackgroundImage:profileImage forState:UIControlStateNormal];
    [userBtn setBackgroundImage:profileImage forState:UIControlStateHighlighted];
    //    [navLogo addTarget:self action:@selector(showPopOver:) forControlEvents:UIControlEventTouchUpInside];
    userBtn.layer.cornerRadius = 18.f;
    userBtn.layer.borderColor = UIColorFromRGB(0x999999).CGColor;
    userBtn.layer.borderWidth = 2.f;
    userBtn.clipsToBounds = YES;
    [navContainer addSubview:userBtn];
    
    UIImage *addImage = [UIImage imageNamed:@"add_nav_button"];
    UIImage *addImageDowon = [UIImage imageNamed:@"add_nav_button"];
    UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(265, 0, 44, 44)];
    addBtn.tintColor = [UIColor grayColor];
    [addBtn setBackgroundImage:addImage forState:UIControlStateNormal];
    [addBtn setBackgroundImage:addImageDowon forState:UIControlStateHighlighted];
    [addBtn addTarget:self action:@selector(createBill:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:addBtn];
    
    
    UIImage *searchImage = [UIImage imageNamed:@"search_nav_button"];
    UIImage *searchImageDown = [UIImage imageNamed:@"search_nav_button"];
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(44, 0, 44, 44)];
    searchBtn.tintColor = [UIColor grayColor];
    [searchBtn setBackgroundImage:searchImage forState:UIControlStateNormal];
    [searchBtn setBackgroundImage:searchImageDown forState:UIControlStateHighlighted];
    //    [searchBtn addTarget:self action:@selector(viewProfile:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:searchBtn];
    
    [self.navigationItem setTitleView:navContainer];
    
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    CGRect tableFrame = _tableView.frame;
    tableFrame.size.height -= 108;
    _tableView.frame = tableFrame;
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setAllowsSelection:YES];
    _tableView.scrollsToTop = YES;
    [self.view addSubview:_tableView];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    CGRect collectionFrame = self.view.frame;
    _collectionView = [[UICollectionView alloc] initWithFrame:collectionFrame collectionViewLayout:layout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [_collectionView registerClass:[CollectionCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    _collectionView.backgroundColor = [UIColor clearColor];
    
    _toolbar = [[UIToolbar alloc] init];
    _toolbar.frame = CGRectMake(0, self.view.frame.size.height - 108, self.view.frame.size.width, 44);
    //    toolbar.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    //    NSMutableArray *items = [[NSMutableArray alloc] init];
    //    [items addObject:[[[UIBarButtonItem alloc] initWith....] autorelease]];
    //    [toolbar setItems:items animated:NO];
    //    [items release];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithTitle:@"toolbar title" style:UIBarButtonItemStylePlain target:self action:nil];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:spaceItem, customItem, spaceItem, nil];
    
    [self setToolbarItems:toolbarItems animated:NO];
    [self.view addSubview:_toolbar];

}

-(void)loadBills:(NSDate*)date{
    
    //GET ONE YEAR AGO
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *addComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate: today];
    [addComponents setTimeZone:[NSTimeZone systemTimeZone]];
    addComponents.year = - 1;
    addComponents.month = + 1;
    NSDate *lastYear = [calendar dateByAddingComponents:addComponents toDate:today options:0];
    
    
    NSDateComponents *startComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate: lastYear];
    [startComponents setTimeZone:[NSTimeZone systemTimeZone]];
    startComponents.day = 1;
    startComponents.hour = 0;
    startComponents.minute = 0;
    startComponents.second = 0;
    lastYear = [calendar dateFromComponents:startComponents];
    
    //GET THIS MONTH TO MAKE SURE LIST STARTS OUT WITH CURRENT MONTH
    NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MMM"];
    NSString *currentMonth = [monthFormatter stringFromDate:[NSDate date]];
    
    //REORDER MONTH ARRAY
    for (NSUInteger i = 0; i < 12; i++) {
        NSString *key = [_monthKeys lastObject];
        [_monthKeys insertObject:key atIndex:0];
        [_monthKeys removeLastObject];
        if([key isEqualToString:currentMonth]){
            break;
        }
    }
    
    //CREATE HOLDERS FOR MONTHLY BILLS
    _months = [[NSMutableDictionary alloc] init];
    for(NSString *month in _monthKeys){
        NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
        NSMutableArray *monthlyBills = [[NSMutableArray alloc] init];
        
        [info setObject:monthlyBills forKey:@"Bills"];
        [info setObject:[NSNumber numberWithFloat:0.f] forKey:@"Total"];
        [info setObject:[NSNumber numberWithFloat:0.f] forKey:@"Paid"];
        [info setObject:[NSNumber numberWithFloat:0.f] forKey:@"Du"];
        [info setObject:[NSNumber numberWithFloat:0.f] forKey:@"Late"];
        
        [_months setObject:info forKey:month];
    }
    
    // SNAG ALL OF THE BILL MODEL OBJECTS
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // GET BILLS FROM ONE YEAR AGO TO TODAY
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"du >= %@ ", lastYear];
    [fetchRequest setPredicate:predicate];
    
    // SORT BY DU DATE
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"du" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bill" inManagedObjectContext:_context];
    
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:&error];
    
    //LOOP THROUGH CORE DATA AND ADD BILLS TO THE LIST BY MONTH
    _bills = [[NSMutableArray alloc] init];
    for (Bill *bill in fetchedObjects) {
        NSDate *billDate = (NSDate*)bill.du;
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        [monthFormatter setDateFormat:@"MMM"];
        NSString *billMonth = [monthFormatter stringFromDate:billDate];
        
        //ADD BILL TO CORRESPONDING MONTH
        [[[_months objectForKey:billMonth] objectForKey:@"Bills"] addObject:bill];
        
        //ADD TOTAL, PAID, DU, LATE TO MONTH INFO
        float total = [[[_months objectForKey:billMonth] objectForKey:@"Total"] floatValue];
        total += [bill.amount floatValue];
        [[_months objectForKey:billMonth] setObject:[NSNumber numberWithFloat:total] forKey:@"Total"];
        
        if(bill.paid){
            float paid = [[[_months objectForKey:billMonth] objectForKey:@"Paid"] floatValue];
            paid += [bill.amount floatValue];
            [[_months objectForKey:billMonth] setObject:[NSNumber numberWithFloat:paid] forKey:@"Paid"];
        }
        
        if(!bill.paid){
            float du = [[[_months objectForKey:billMonth] objectForKey:@"Du"] floatValue];
            du += [bill.amount floatValue];
            [[_months objectForKey:billMonth] setObject:[NSNumber numberWithFloat:du] forKey:@"Du"];
        }
        
        
        //CONSIDER A BILL LATE IF IT IS NOW
        //PAST THE DU DATE
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *addComponents = [calendar components:(NSMonthCalendarUnit | NSDayCalendarUnit) fromDate: bill.du];
        [addComponents setTimeZone:[NSTimeZone systemTimeZone]];
        addComponents.day = + 1;
        addComponents.month = + 1;
        NSDate *lateDate = [calendar dateByAddingComponents:addComponents toDate:bill.du options:0];
        //OVERRIDE TO DU DATE RATHER THAN 30 DAYS
        lateDate = bill.du;
        if([[NSDate date] compare:lateDate] == NSOrderedDescending){
            float late = [[[_months objectForKey:billMonth] objectForKey:@"Late"] floatValue];
            late += [bill.amount floatValue];
            [[_months objectForKey:billMonth] setObject:[NSNumber numberWithFloat:late] forKey:@"Late"];
        }
        
        _headerChartValues = [@[[[_months objectForKey:billMonth] objectForKey:@"Du"], [[_months objectForKey:billMonth] objectForKey:@"Du"]] mutableCopy];
        
        [_bills addObject:bill];
    }
}

-(void)createBill:(id)sender{
    DUCreateBillViewController *createViewController = [[DUCreateBillViewController alloc] init];
    createViewController.delegate = self;
    createViewController.view.backgroundColor = [UIColor clearColor];
    createViewController.title = @"New Bill";
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:createViewController];
    [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _toolbar.alpha  = 0;
        _tableView.alpha = 0;
    } completion:^(BOOL finished) {
        [self presentViewController:navController animated:YES completion:nil];
    }];
}

-(void)didCancelCreateBill:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            _toolbar.alpha  = 1;
            _tableView.alpha = 1;
        } completion:nil];

    }];
}
-(void)didCreateBill:(id)sender withBill:(Bill *)bill{
    [_bills addObject:bill];
    [_tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:^{
        [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            _toolbar.alpha  = 1;
            _tableView.alpha = 1;
        } completion:nil];
    }];
}

- (void)buttonTapped:(id)sender
{
    [_collectionView setHidden:NO];
    [_tableView setHidden:NO];
    [_snap removeFromSuperview];
    UIView *fromView, *toView;
    
    if (self.tableView.superview == self.view)
    {
        fromView = self.tableView;
        toView = self.collectionView;
    }
    else
    {
        fromView = self.collectionView;
        toView = self.tableView;
    }
    CGRect toViewFrame = toView.frame;
    toView.frame = toViewFrame;
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.25
                       options:UIViewAnimationOptionTransitionNone
                    completion:nil];
}

#pragma mark UITableViewDelegate Methods

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSMutableDictionary *monthInfo = [_months objectForKey:[_monthKeys objectAtIndex: section]];
    
    BillHeader *headerView = [[BillHeader alloc] init];
    headerView.monthLabel.text = [[_monthKeys objectAtIndex:section] uppercaseString];
    headerView.totalAmountLabel.text = [NSString stringWithFormat:@"$%@", [monthInfo objectForKey:@"Total"]];
    headerView.paidAmountLabel.text = [NSString stringWithFormat:@"$%@", [monthInfo objectForKey:@"Paid"]];
    headerView.dueAmountLabel.text = [NSString stringWithFormat:@"$%@", [monthInfo objectForKey:@"Du"]];
    headerView.lateAmountLabel.text = [NSString stringWithFormat:@"$%@", [monthInfo objectForKey:@"Late"]];
    headerView.pieChart.datasource = self;
    headerView.pieChart.delegate = self;
    NSLog(@"Du: %@", [[_months objectForKey:[_monthKeys objectAtIndex:section]] objectForKey:@"Du"]);
    if([[_months objectForKey:[_monthKeys objectAtIndex:section]] objectForKey:@"Du"]){
        _headerChartValues = [@[[[_months objectForKey:[_monthKeys objectAtIndex:section]] objectForKey:@"Paid"], [[_months objectForKey:[_monthKeys objectAtIndex:section]] objectForKey:@"Du"]] mutableCopy];
    }

    [headerView.pieChart reloadData];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 120.f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001f;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([_monthKeys count] > 0){
        return [_monthKeys count];
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NUMBER OF ROWS EQUALS THE NUMBER OF BILLS FOR
    //THE MONTH BEING DISPLAYED
    if([[[_months objectForKey:[_monthKeys objectAtIndex:section]] objectForKey:@"Bills"] count] > 0){
        return [[[_months objectForKey:[_monthKeys objectAtIndex:section]] objectForKey:@"Bills"] count];
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    //DISPLAY BILLS IF THERE ARE ANY FOR THE CURRENT MONTH
    if([[[_months objectForKey:[_monthKeys objectAtIndex:[indexPath section]]] objectForKey:@"Bills"] count] > 0){
        static NSString *CellIdentifier = @"bill";
        Cell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            cell = [[Cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        Bill *bill = [[[_months objectForKey:[_monthKeys objectAtIndex:[indexPath section]]] objectForKey:@"Bills"] objectAtIndex:indexPath.row];
        Payee *payee = (Payee*)bill.payee;
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        [monthFormatter setDateFormat:@"MMM. dd"];
        NSString *duDate = [monthFormatter stringFromDate:bill.du];
        
        cell.isPaid = (bill.paid) ? YES : NO;
        cell.parentTableView = tableView;
        cell.categoryImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_cell_icon", [payee.category lowercaseString]]];
        cell.payeeLabel.text = payee.name;
        cell.amountLabel.text = [NSString stringWithFormat:@"%.02f", [bill.amount floatValue]];
        cell.dueDateLabel.text = duDate;
        cell.userImageView.image = [UIImage imageNamed:@"profile_image.jpg"];
        cell.userLabel.text = @"Created By Daniel Burke";
        cell.delegate = self;
        return cell;
    }
    
    //OTHERWISE DISPLAY 'No Bills' MESSAGE
    //TODO: ADD NEW BILL BUTTON?
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    return cell;
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return 2;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    if(index == 0){
        return 40;
    }
    return 60;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    if(index == 0){
        return UIColorFromRGB(0x3399ff);
    }
    return UIColorFromRGB(0xcc9999);
}

#pragma mark BillCellDelegate methods
-(void)didToggleBillPaid:(UIButton*)button{
    Cell *cell = (Cell*)button.superview.superview.superview.superview;
    NSIndexPath *path = [_tableView indexPathForCell:cell];
    NSInteger section = [path section];
    NSString *billMonth = [_monthKeys objectAtIndex:section];
    NSMutableArray *bills = [[_months objectForKey:billMonth] objectForKey:@"Bills"];
    NSError *error = nil;
    Bill *bill = [bills objectAtIndex:path.row];
    
    if(button.isSelected){
        [button setSelected:NO];
        bill.paid = nil;
    }
    else{
        [button setSelected:YES];
        bill.paid = [NSDate date];
    }
    [bills setObject:bill atIndexedSubscript:path.row];

    [[_months objectForKey:billMonth] setObject:bills forKey:@"Bills"];
    [_context save:&error];
    
    //ALSO UPDATE THE BILL MODEL AND SAVE
    //    [_tableView reloadData];
    [_tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:[path section]];
    [self loadBills:[NSDate date]];
    [_tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

-(void)didTapCell:(id)sender{
    DUBillViewController *billViewController = [[DUBillViewController alloc] init];
    billViewController.view.backgroundColor = [UIColor whiteColor];
    billViewController.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationController pushViewController:billViewController animated:YES];
}

-(void)didDeleteCell:(NSIndexPath*)path{
    
    //REMOVE BILL FROM BILLS ARRAY
    NSInteger section = [path section];
    NSString *billMonth = [_monthKeys objectAtIndex:section];
    NSMutableArray *bills = [[_months objectForKey:billMonth] objectForKey:@"Bills"];
    
    //REMOVE FROM CORE DATA
    NSError *error = nil;
    Bill *deleteBill = [bills objectAtIndex:path.row];
    [_context deleteObject:deleteBill];
    [_context save:&error];
    
    [bills removeObjectAtIndex:path.row];
    
    //RESAVE BILLS TO ARRAY
    [[_months objectForKey:billMonth] setObject:bills forKey:@"Bills"];
    [_tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
    
    //UPDATE HEADER SECTION DATA
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:[path section]];
    [self loadBills:[NSDate date]];
    [_tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - XYPieChart Delegate
- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    NSLog(@"did select slice at index %@", [NSNumber numberWithInteger:index]);
}

#pragma mark -    PieChartViewDelegate
-(CGFloat)centerCircleRadius
{
    return 45.f;
}
#pragma mark - PieChartViewDataSource
-(int)numberOfSlicesInPieChartView:(PieChartView *)pieChartView
{
    return 2;
}

-(UIColor *)pieChartView:(PieChartView *)pieChartView colorForSliceAtIndex:(NSUInteger)index
{
//    return GetRandomUIColor();
    if(index == 0){
        return [[UIColor whiteColor] colorWithAlphaComponent:1];
    }
    return [[UIColor whiteColor] colorWithAlphaComponent:0.5];
}

-(double)pieChartView:(PieChartView *)pieChartView valueForSliceAtIndex:(NSUInteger)index
{
    return [[_headerChartValues objectAtIndex:index] doubleValue];
}

#pragma mark UICollectionViewDelegate Methods

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    CGRect rectInTableView = cell.frame;
    CGRect rectInSuperview = [_collectionView convertRect:rectInTableView toView:[_collectionView superview]];
    
    _snap = [[UIView alloc] init];
    _snap = [_collectionView snapshotViewAfterScreenUpdates:YES];
    [_collectionView setHidden:YES];
    [self.view addSubview:_snap];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect snapFrame = _snap.frame;
        snapFrame.origin.x -= 5;
        snapFrame.size.height = 2.1 * snapFrame.size.height;
        snapFrame.size.width = 2 * snapFrame.size.width;
        _snap.frame = snapFrame;
    } completion:^(BOOL finished) {
        //        [_snap removeFromSuperview];
    }];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(145, 180);
}
@end
