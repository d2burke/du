//
//  Note.m
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "Note.h"
#import "Bill.h"
#import "RBill.h"


@implementation Note

@dynamic text;
@dynamic created;
@dynamic synced;
@dynamic bill;
@dynamic rbill;
@dynamic user;

@end
