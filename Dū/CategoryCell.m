//
//  CategoryCell.m
//  Dū
//
//  Created by Daniel Burke on 12/23/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        _icon.image = [UIImage imageNamed:@"phone_cell_icon"];
        _label = [[UILabel alloc] initWithFrame:CGRectMake(43, 0, 277, 40)];
        _label.tag = 1;
        _label.textColor = [UIColor grayColor];
        _label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
        [self addSubview:_label];
        [self addSubview:_icon];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
