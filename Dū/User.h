//
//  User.h
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bill;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * pin;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * facebookid;
@property (nonatomic, retain) NSString * twitterid;
@property (nonatomic, retain) NSDate * synced;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) Bill *bills;

@end
