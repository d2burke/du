//
//  DUCreateBillViewController.m
//  Dū
//
//  Created by Daniel Burke on 12/14/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "DUCreateBillViewController.h"
#import "UIImage+ImageEffects.h"
#import "IconCollectionCell.h"

@interface DUCreateBillViewController ()

@end

@implementation DUCreateBillViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _reminders = [[NSMutableArray alloc] init];
    _recurringTypes = @[@"Once", @"Daily", @"BiMonthly", @"Monthly", @"Annually"];
    _recurringType = kRecurringTypeOnce;
    
    
    //GET GLOBAL SINGLETON CONTEXT
    _appDelegate = (DUAppDelegate *)[[UIApplication sharedApplication] delegate];
    _context = [_appDelegate managedObjectContext];
    
    [self getPayees];
    [self initDefaults];
    [self initNav];
    [self initUI];
}

-(void)viewWillAppear:(BOOL)animated{
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    
    self.navigationController.navigationBar.barTintColor = [[UIColor whiteColor] colorWithAlphaComponent:1];  // UIColorFromRGB(0xb25538);
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x999999);
    self.navigationController.navigationBar.translucent = NO;
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    
    [navigationBar setBackgroundImage:[UIImage new]
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    
    [navigationBar setShadowImage:[UIImage new]];
    navigationBar.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancelCreate:(id)sender{
    [_delegate didCancelCreateBill:self];
}

-(void)create:(id)sender{
    NSError *error = nil;
    if(_createNewPayee){
        Payee *payee = [NSEntityDescription insertNewObjectForEntityForName:@"Payee" inManagedObjectContext:_context];
        payee.name = _payeeName;
        payee.category = _categoryName;
        
        //SAVE NEW PAYEE TO CORE DATA STORE
        [_context save:&error];
        
        //TODO: SYNC NEW PAYEE TO SERVER
        //MAKE CALL TO SERVER TO SAVE NEW PAYEE ON SERVER
        //WHEN CALL COMES BACK, UPDATE 'synced' ATTRIBUTE
        //AND RESAVE THIS PAYEE TO CORE DATA
        
        //ADD NEW PAYEE TO ARRAY TO POPULATE PAYEE PICKER TABLEVIEW
        [_payees addObject:payee];
        
        _chosen_payee = payee;
    }
    
    if (!_chosen_payee){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Create Error" message:@"Oops, looks like you're missing a Payee." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if (![_amountTextField.text length]){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Create Error" message:@"Oops, looks like you're missing an Amount." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //CREATE NEW BILL WITH INFO
    Bill *bill = [NSEntityDescription insertNewObjectForEntityForName:@"Bill" inManagedObjectContext:_context];
    bill.category = _categoryName;
    bill.amount = [NSNumber numberWithFloat:[_amountTextField.text floatValue]];
    bill.du = _dateChosen;
    bill.paid = ([_checkButton isSelected]) ? [NSDate date] : nil;
    bill.created = [NSDate date];
//    bill.synced =
//    bill.user = 
    if([_notesTextView.text length]){
        Note *note = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:_context];
        note.text = _notesTextView.text;
        note.created = [NSDate date];
        bill.notes = note;
    }
//    bill.reminders = 
    bill.payee = _chosen_payee;
    NSLog(@"New Bill: %@", bill);
    [_context save:&error];
    [_delegate didCreateBill:self withBill:bill];
}

-(void)toggleBillPaid{
    [self.view endEditing:YES];
    if (_checkButton.isSelected) {
        _checkButton.selected = NO;
        _paidLabel.text = @"Unpaid";
        //TOGGLE BILL unPAID
    }
    else{
        _checkButton.selected = YES;
        _paidLabel.text = @"Paid";
        //TOGGLE BILL PAID
    }
}

-(void)showRecurringOptions{
    [self.view endEditing:YES];
    if (_recurringType == 4) {
        _recurringType = 0;
    }
    else{
        _recurringType++;
    }
    if (_recurringType == kRecurringTypeOnce) {
        _recurringTypeTextLabel.text = @"Happens";
        [_recurringButton setSelected:NO];
    }
    else{
        _recurringTypeTextLabel.text = @"Recurs";
        [_recurringButton setSelected:YES];
    }
    _recurringTypeLabel.text = [_recurringTypes objectAtIndex:_recurringType];
}

#pragma mark ScrollView Delegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == _tableView) {
//        if(scrollView.contentOffset.y > 15 || scrollView.contentOffset.y < -15){
//            [self.view endEditing:YES];
//        }
        if((-1*scrollView.contentOffset.y) > 0){
            CGFloat adjustment;
            if ((-1*scrollView.contentOffset.y) < 40) {
                adjustment = (-1.2*scrollView.contentOffset.y) / 2;
            }
            else{
                adjustment = (-1*scrollView.contentOffset.y) / 2 + 22;
            }
            adjustment = (-1.45*scrollView.contentOffset.y) / 2;
//            NSLog(@"%0.2f", scrollView.contentOffset.y);
            UIButton *navLogo = (UIButton*)[[self.navigationItem.titleView subviews] objectAtIndex:0];
            CGRect buttonFrame = navLogo.frame;
            buttonFrame.origin.y = adjustment;
//            navLogo.frame = buttonFrame;
        }
    }
}



#pragma mark ReminderCell Delegate
-(void)didDeleteReminder:(NSIndexPath *)path{
    ReminderCell *cell = (ReminderCell*)[_tableView cellForRowAtIndexPath:path];
    [_reminders removeObject:[_reminderTypes objectAtIndex:cell.reminderType]];
    
    NSIndexPath *addPath = [NSIndexPath indexPathForRow:0 inSection:1];
    ReminderCell *addCell = (ReminderCell*)[_tableView cellForRowAtIndexPath:addPath];
    [addCell.reminderButton addTarget:self action:@selector(showReminderPicker:) forControlEvents:UIControlEventTouchUpInside];
    addCell.reminderButton.alpha = 1.f;
    
    [_tableView beginUpdates];
    [_tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationTop];
    [_tableView endUpdates];
}

#pragma mark UITextView Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self hideCalendar];
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *payeeCell = [_tableView cellForRowAtIndexPath:path];
    UITextField *payeeTextField = (UITextField*)[payeeCell viewWithTag:2];
    
    if (textField == payeeTextField) {
        if([_payeeSearchMatches count]){
            CGRect selectionViewFrame = self.view.bounds;
            selectionViewFrame.size.height -= 184;
            selectionViewFrame.origin.y = 74.f;
            _payeeView.frame = selectionViewFrame;
            [_payeeView setHidden:NO];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *payeeCell = [_tableView cellForRowAtIndexPath:path];
    UITextField *payeeTextField = (UITextField*)[payeeCell viewWithTag:2];
    if(textField == payeeTextField){
        [_payeeView setHidden:YES];
    }
    
    if (textField == _amountTextField && [_amountTextField.text isEqualToString:@"0.00"]) {
        _amountTextField.text = @"";
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([textView.text length] == 0) {
        textView.textColor = UIColorFromRGB(0x999999);
    }
    else{
        textView.textColor = [UIColor grayColor];
    }
    if([text isEqualToString:@"\n"]){
//        [textView resignFirstResponder];
    }
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@"Type Note"]){
        textView.text = @"";
    }
    [UIView animateWithDuration:0.33f delay:0.5f options:UIViewAnimationOptionCurveEaseIn animations:^{
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: 0 inSection: 2];
        [_tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    } completion:nil];
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        textView.textColor = UIColorFromRGB(0x999999);
        textView.text = @"Type Note";
    }
    return YES;
}

-(void)keyboardWillHide:(NSNotification*)n{
    CGRect tableFrame = _tableView.frame;
    tableFrame.size.height = self.view.frame.size.height;
    _tableView.frame = tableFrame;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    [UIView animateWithDuration:0.34f animations:^{
        CGRect tableFrame = _tableView.frame;
        CGSize keyboardSize = [[[n userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        tableFrame.size.height = self.view.frame.size.height - keyboardSize.height - 4;
        _tableView.frame = tableFrame;
    }];
}

-(void)keyboardDidHide:(NSNotification*)n{
//    [UIView animateWithDuration:1 animations:^{
//        NSIndexPath* ipath = [NSIndexPath indexPathForRow: 0 inSection: 0];
//        [_tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
//    }];
}

- (void)keyboardDidShow:(NSNotification *)n{

}

-(void)textViewDidChange:(UITextView *)textView{
    
}

-(void)formatDollarAmount:(UITextField*)textField{
    static BOOL toggle = NO;
    if(toggle){
        toggle = YES;
        return;
    }
    
    NSString *strPrice = textField.text;
    strPrice = [strPrice stringByReplacingOccurrencesOfString:@"." withString:@""];
    textField.text = [@"" stringByAppendingFormat:@"%0.2f", [strPrice floatValue]/100.0];
    
}

-(void)searchPayees:(UITextField*)textField{
    [_payeeView setHidden:NO];
    [_iconView setHidden:YES];
    [_categoryView setHidden:YES];
    _createNewPayee = NO;
    
    UITableViewCell *cell = (UITableViewCell*)[textField superview];
    UILabel *label = (UILabel*)[cell viewWithTag:5];
    [label setHidden:YES];
    
    //SEARCH PAYEES AND RELOAD _payeeTableView
    NSMutableArray *matches = [[NSMutableArray alloc] init];
    BOOL found = NO;
    for (Payee *payee in _payees)
    {
        if ([payee.name rangeOfString:textField.text options:NSCaseInsensitiveSearch].location != NSNotFound){
            [matches addObject:payee];
            found = YES; //AT LEAST ONE FOUND
        }
    }
    
    
    if (found) {
        _payeeSearchMatches = [matches mutableCopy];
        NSLog(@"Find: %@", textField.text);
        NSLog(@"In: %@", _payeeSearchMatches);
        [_payeeTableView reloadData];
    }
    else{
        _payeeSearchMatches = [_payees mutableCopy];
        NSLog(@"Reset: %@", _payeeSearchMatches);
        [_payeeView setHidden:YES];
        
        if([textField.text length] > 0){
            //THIS IS A NEW PAYEE
            _payeeName = textField.text;
            [label setHidden:NO];
            _createNewPayee = YES;
            [_categoryView setHidden:NO];
        }
    }
}

#pragma mark UITableView Delegate Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _payeeTableView){
        _chosen_payee = [_payeeSearchMatches objectAtIndex:indexPath.row];
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewCell *payeeCell = [_tableView cellForRowAtIndexPath:path];
        UIImageView *payeeIcon = (UIImageView*)[payeeCell viewWithTag:1];
        UITextField *payeeTextField = (UITextField*)[payeeCell viewWithTag:2];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UILabel *label = (UILabel*)[cell viewWithTag:1];

        NSLog(@"Selection: %@", _chosen_payee);
        payeeTextField.text = label.text;
        [payeeTextField resignFirstResponder];
        payeeIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_cell_icon", [_chosen_payee.category lowercaseString]]];
        
        //RECORD PAYEE NAME FOR LATER USER
        _payeeName = label.text;
        
        //HIDE PICKER
        [_payeeView setHidden:YES];
        
        //RESET PAYEE SEARCH MATCHES
        _payeeSearchMatches = [_payees mutableCopy];
        
        //REFRESH PAYEE PICKER TO REFLECT RESET ON NEXT SEARCH
        [_payeeTableView reloadData];
    }
    else if(tableView == _categoryTableView){
        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewCell *payeeCell = [_tableView cellForRowAtIndexPath:path];
        UIImageView *payeeIcon = (UIImageView*)[payeeCell viewWithTag:1];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UILabel *label = (UILabel*)[cell viewWithTag:1];
        NSLog(@"Selection: %@", label.text);
        payeeIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_cell_icon", [label.text lowercaseString]]];
        _categoryName = label.text;
        [_categoryView setHidden:YES];
    }
    else if(tableView == _reminderTableView){
        NSLog(@"Remind me %@", [_reminderTypes objectAtIndex:indexPath.row]);
        [self addReminder:indexPath withDescription:[_reminderTypes objectAtIndex:indexPath.row]];
    }
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == _payeeTableView) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width, 30)];
        headerTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        headerTitle.textColor = [UIColor whiteColor];
        headerTitle.text = @"Who's the money going to?";
        
        [headerView addSubview:headerTitle];
        return headerView;
    }
    else if (tableView == _categoryTableView) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width, 30)];
        headerTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        headerTitle.textColor = [UIColor whiteColor];
        headerTitle.text = @"What sort of bill is this?";
        
        [headerView addSubview:headerTitle];
        return headerView;
    }
    else if(tableView == _reminderTableView){
        return nil;
    }
    else{
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        headerView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width, 30)];
        headerTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        headerTitle.textColor = [UIColor whiteColor];
        
        switch (section) {
            case 0:
                headerTitle.text = @"Create a new bill";
                break;
            case 1:
                headerTitle.text = @"Remind yourself";
                break;
            case 2:
                headerTitle.text = @"Add some notes";
                break;
            default:
                break;
        }
        
        [headerView addSubview:headerTitle];
        return headerView;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == _reminderTableView){
        return 0.000001f;
    }
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001f;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == _payeeTableView || tableView == _categoryTableView || tableView == _reminderTableView) {
        return 1;
    }
    else{
        return 3;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == _tableView){
        if (section == 0) {
            return 3;
        }
        else if(section == 1){
            return [_reminders count] + 1;
        }
        return 1;
    }
    else if(tableView == _payeeTableView && [_payeeSearchMatches count] > 0){
        return [_payeeSearchMatches count];
    }
    else if(tableView == _categoryTableView && [_categorySearchMatches count] > 0){
        return [_categorySearchMatches count];
    }
    else if(tableView == _reminderTableView){
        return [_reminderTypes count];
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _payeeTableView || tableView == _categoryTableView || tableView == _reminderTableView){
        return 40;
    }
        else{
        switch ([indexPath section]) {
            case 0:
            case 1:{
                return 46;
            }
                break;
            case 2:{
                return 120;
            }
                break;
        }
        return 46;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (tableView == _categoryTableView || tableView == _payeeTableView){
        static NSString *CategroyIdentifier = @"category";
        CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CategroyIdentifier];
        if(cell == nil){
            cell = [[CategoryCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CategroyIdentifier];
        }
        
        if (tableView == _payeeTableView) {
            if([_payeeSearchMatches count]){
                Payee *payee = (Payee*)[_payeeSearchMatches objectAtIndex:indexPath.row];
                cell.label.text = payee.name;
                cell.icon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_cell_icon", [payee.category lowercaseString]]];
            }
        }
        else if (tableView == _categoryTableView) {
            cell.icon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_cell_icon",[[_categorySearchMatches objectAtIndex:indexPath.row] lowercaseString]]];
            cell.label.text = [_categorySearchMatches objectAtIndex:indexPath.row];
        }
        return cell;
    }
    else if (tableView == _reminderTableView){
        static NSString *CategroyIdentifier = @"reminder";
        ReminderTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:CategroyIdentifier];
        if(cell == nil){
            cell = [[ReminderTypeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CategroyIdentifier];
        }
        if([_reminders containsObject:[_reminderTypes objectAtIndex:indexPath.row]]){
            cell.label.textColor = UIColorFromRGB(0xCCCCCC);
            [cell setUserInteractionEnabled:NO];
        }
        else{
            cell.label.textColor = [UIColor grayColor];
            [cell setUserInteractionEnabled:YES];
        }
        cell.label.text = [_reminderTypes objectAtIndex:indexPath.row];
        return cell;
    }
    else if(tableView == _tableView){
        static NSString *CellIdentifier = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        switch ([indexPath section]) {
            case 0:{
                switch (indexPath.row) {
                    case 0:{
                        static NSString *CellIdentifier = @"payee";
                        PayeeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                        if(cell == nil){
                            cell = [[PayeeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                        }
                        cell.payeeTextField.delegate = self;
                        [cell.payeeTextField addTarget:self action:@selector(searchPayees:) forControlEvents:UIControlEventEditingChanged];
                        return cell;
                        break;
                    }
                    case 1:{
                        static NSString *CellIdentifier = @"amount";
                        AmountDueCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                        if(cell == nil){
                            cell = [[AmountDueCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                        }
                        
                        _amountTextField = cell.amountTextField;
                        _dayLabel = cell.dayLabel;
                        _monthLabel = cell.monthLabel;
                        _yearLabel = cell.yearLabel;
                        
                        _dateChosen = [NSDate date];
                        _amountTextField.delegate = self;
                        [_amountTextField addTarget:self action:@selector(formatDollarAmount:) forControlEvents:UIControlEventEditingChanged];
                        
                        [cell.dueButton addTarget:self action:@selector(showCalendar:) forControlEvents:UIControlEventTouchUpInside];
                        return cell;
                        break;
                    }
                    case 2:{
                        static NSString *CellIdentifier = @"paid";
                        PaidRecurringCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                        if(cell == nil){
                            cell = [[PaidRecurringCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                        }
                        _paidLabel = cell.paidLabel;
                        _checkButton = cell.checkButton;
                        _recurringButton = cell.recurringButton;
                        _recurringTypeLabel = cell.recurringTypeLabel;
                        _recurringTypeTextLabel = cell.recurringTypeTextLabel;
                        
                        [_checkButton addTarget:self action:@selector(toggleBillPaid) forControlEvents:UIControlEventTouchUpInside];
                        [_recurringButton addTarget:self action:@selector(showRecurringOptions) forControlEvents:UIControlEventTouchUpInside];
                        return cell;
                        break;
                    }
                }
                break;
            }
            case 1:{
                    static NSString *CellIdentifier = @"reminder";
                    ReminderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if(cell == nil){
                        cell = [[ReminderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                    }
                    if(([_reminders count] > 0 && indexPath.row > 0)){
                        cell.delegate = self;
                        cell.accessoryType = UITableViewCellAccessoryNone;
                        
                        //ADJUST COLORS AND ACTIONS FOR REMINDERS
                        cell.reminderIcon.image = [UIImage imageNamed:@"reminder_cell_icon"];
                        cell.reminderLabel.text = [NSString stringWithFormat:@"Reminder %d", indexPath.row];
                        CGRect labelFrame = cell.reminderLabel.frame;
                        labelFrame.origin.y = 6;
                        cell.reminderLabel.frame = labelFrame;
                        cell.reminderLabel.textColor = UIColorFromRGB(0x66CC66);
                        [cell.reminderLabel setHidden:NO];
                        
                        CGRect buttonframe = cell.reminderButton.frame;
                        buttonframe.origin.y = 6;
                        cell.reminderButton.frame = buttonframe;
                        int index = [_reminderTypes indexOfObject:[_reminders objectAtIndex:(indexPath.row - 1)]];
                        cell.reminderType = index;
                        NSString *reminderDescription = [NSString stringWithFormat:@"Remind me %@", [_reminders objectAtIndex:(indexPath.row - 1)]];
                        [cell.reminderButton setTitle:reminderDescription forState:UIControlStateNormal];
                        [cell.reminderButton setTitleColor:UIColorFromRGB(0x66CC66) forState:UIControlStateNormal];
                        [cell.reminderButton removeTarget:self action:@selector(showReminderPicker:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else{
                        cell = [[ReminderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                        //SET DEFAULTS FOR Add Reminder BUTTON
                        [cell.scrollView setContentSize:CGSizeMake(320, 44)];
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.reminderIcon.image = [UIImage imageNamed:@"clock_cell_icon"];
                        [cell.reminderButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                        if([_reminders count] < 3){
                            [cell.reminderButton addTarget:self action:@selector(showReminderPicker:) forControlEvents:UIControlEventTouchUpInside];
                            cell.reminderButton.alpha = 1.f;
                        }
                        else{
                            [cell.reminderButton removeTarget:self action:@selector(showReminderPicker:) forControlEvents:UIControlEventTouchUpInside];
                            cell.reminderButton.alpha = 0.3f;
                        }
                    }
                    return cell;
                    break;
            }
            case 2:{
                static NSString *CellIdentifier = @"notes";
                NotesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if(cell == nil){
                    cell = [[NotesCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                }
                cell.notesTextView.delegate = self;
                return cell;
                break;
            }
                
            default:
                break;
        }
        return cell;
    }
    return cell;
}

#pragma mark UICollectionViewDelegate Methods

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //SET PAYEE ICON IMAGEVIEW
    NSLog(@"Chose: %@", [_iconCatalog objectAtIndex:indexPath.row]);
    _categoryIcon = [_iconCatalog objectAtIndex:indexPath.row];
    [_iconView setHidden:YES];
    [self.view endEditing:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_iconCatalog count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Icon: %@", [NSString stringWithFormat:@"%@_bill_icon", [_iconCatalog objectAtIndex:indexPath.row]]);
    IconCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor clearColor];
    cell.icon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_bill_icon", [_iconCatalog objectAtIndex:indexPath.row]]];
    cell.icon.contentMode = UIViewContentModeScaleAspectFill;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(44, 44);
}

#pragma mark CalendarPicker Delegate Methods
-(void)didSelectDate:(NSDate *)date{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didCancelSelectDate:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showReminderPicker:(id)sender{
    [self.view endEditing:YES];
    if(_datePickerIsShowing){
        [self hideCalendar];
    }
    if(_reminderPickerIsShowing){
        return;
    }
    
    [_tableView setUserInteractionEnabled:NO];
    _reminderPickerIsShowing = YES;
    CGRect pickerFrame = CGRectMake(0, self.view.frame.size.height - 216 - 64, self.view.frame.size.width, 216);
    _pickerView = [[UIView alloc] initWithFrame:pickerFrame];
    _pickerView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    pickerFrame.origin.y = self.view.frame.size.height;
    _pickerView.frame = pickerFrame;
    
    _reminderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 172) style:UITableViewStylePlain];
    _reminderTableView.backgroundColor = [UIColor whiteColor];
    [_reminderTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    _reminderTableView.delegate = self;
    _reminderTableView.dataSource = self;
    [_reminderTableView setAllowsSelection:YES];
    [_pickerView addSubview:_reminderTableView];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cancelButton.frame = CGRectMake(250, 0, 65, 44);
    [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    _cancelButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    
    [_pickerView addSubview:_cancelButton];
    [_pickerView addSubview:_reminderTableView];
    [self.view addSubview:_pickerView];
    
    [UIView animateWithDuration:0.2f animations:^{
        CGRect tableFrame = _tableView.frame;
        tableFrame.size.height = self.view.frame.size.height - 216.f - 4;
        _tableView.frame = tableFrame;
        
        CGRect pickerFrame = _pickerView.frame;
        pickerFrame.origin.y = self.view.frame.size.height - 216;
        _pickerView.frame = pickerFrame;
        
        [_cancelButton addTarget:self action:@selector(hideReminderPicker:) forControlEvents:UIControlEventTouchUpInside];
        
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: 0 inSection: 2];
        [_tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }];
}

-(void)hideReminderPicker:(id)sender{
    NSIndexPath* ipath = [NSIndexPath indexPathForRow: 0 inSection: 0];
    [_tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: NO];
    
    [UIView animateWithDuration:0.34f animations:^{
        [_tableView setUserInteractionEnabled:YES];
        CGRect tableFrame = _tableView.frame;
        tableFrame.size.height = self.view.frame.size.height;
        _tableView.frame = tableFrame;
        
        CGRect pickerFrame = _pickerView.frame;
        pickerFrame.origin.y = self.view.frame.size.height;
        _pickerView.frame = pickerFrame;
    } completion:^(BOOL finished) {
        _reminderPickerIsShowing = NO;
    }];
}

-(void)addReminder:(id)sender withDescription:(NSString*)description{
    if ([_reminders count] < 3) {
        [_reminders addObject:description];
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:[_reminders count] inSection:1];
        
        //ADD ROW AFTER PICKERVIEW HIDES
        [self hideReminderPicker:self];
        [_tableView reloadData];
//        [_tableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationTop];
    }


}

-(void)showCalendar:(id)sender{
    //HIDE REMINDER PICKER
    if(_reminderPickerIsShowing){
        [self hideReminderPicker:self];
    }
    
    if(!_datePickerIsShowing){
        [self showDatePicker:sender];
    }
    else{
        [self dateChosen:sender];
    }
    [self.view endEditing:YES];
}

-(void)dateChanged:(UIDatePicker *)sender{
    _dateChosen = [sender date];
    
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter setDateFormat:@"dd"];
    NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MMM"];
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"y"];
    
    //    _dayLabel = (UILabel*)[cell viewWithTag:1];
    _dayLabel.text = [dayFormatter stringFromDate:[sender date]];
    //    _monthLabel = (UILabel*)[cell viewWithTag:2];
    _monthLabel.text = [monthFormatter stringFromDate:[sender date]];
    //    _yearLabel = (UILabel*)[cell viewWithTag:3];
    _yearLabel.text = [yearFormatter stringFromDate:[sender date]];
}

-(void)showDatePicker:(id)sender{
    
    [self.view endEditing:YES];
    _datePickerIsShowing = YES;
    
    // create a UIPicker view as a custom keyboard view
    _datePicker = [[UIDatePicker alloc] init];
    [_datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [_datePicker sizeToFit];
    _datePicker.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _datePicker.backgroundColor = [UIColor whiteColor];
    CGRect pickerFrame = _datePicker.frame;
    CGRect dateFrame = _datePicker.frame;
    dateFrame.size.height -= 36;
    dateFrame.origin.y = 44;
    _datePicker.frame = dateFrame;
    
    _pickerView = [[UIView alloc] initWithFrame:pickerFrame];
    _pickerView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    pickerFrame.origin.y = self.view.frame.size.height;
    _pickerView.frame = pickerFrame;
    
    _chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _chooseButton.frame = CGRectMake(250, 0, 65, 44);
    [_chooseButton setTitle:@"Done" forState:UIControlStateNormal];
    [_chooseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_chooseButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    _chooseButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    
    [_pickerView addSubview:_chooseButton];
    [_pickerView addSubview:_datePicker];
    [self.view addSubview:_pickerView];
    
    [UIView animateWithDuration:0.2f animations:^{
        CGRect tableFrame = _tableView.frame;
        tableFrame.size.height = self.view.frame.size.height - 216.f - 4;
        _tableView.frame = tableFrame;
        
        CGRect pickerFrame = _pickerView.frame;
        pickerFrame.origin.y = self.view.frame.size.height - 216;
        _pickerView.frame = pickerFrame;
        
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_chooseButton addTarget:self action:@selector(dateChosen:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(void)hideCalendar{
    _datePickerIsShowing = NO;
    [UIView animateWithDuration:0.2f animations:^{
        CGRect tableFrame = _tableView.frame;
        tableFrame.size.height = self.view.frame.size.height;
        _tableView.frame = tableFrame;
        
        CGRect pickerFrame = _pickerView.frame;
        pickerFrame.origin.y = self.view.frame.size.height;
        _pickerView.frame = pickerFrame;
    }];
}

-(void)dateChosen:(id)sender{
    [self hideCalendar];
    
    _dateChosen = [_datePicker date];
    
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter setDateFormat:@"dd"];
    NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MMM"];
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"y"];
    
//    _dayLabel = (UILabel*)[cell viewWithTag:1];
    _dayLabel.text = [dayFormatter stringFromDate:[_datePicker date]];
//    _monthLabel = (UILabel*)[cell viewWithTag:2];
    _monthLabel.text = [monthFormatter stringFromDate:[_datePicker date]];
    NSLog(@"Month: %@", [monthFormatter stringFromDate:[_datePicker date]]);
//    _yearLabel = (UILabel*)[cell viewWithTag:3];
    _yearLabel.text = [yearFormatter stringFromDate:[_datePicker date]];
    NSLog(@"Year: %@", [yearFormatter stringFromDate:[_datePicker date]]);
}

//INITIATE UI
-(void)initUI{
    
    _bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nz"]];
    _bg.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    _bg.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_bg];
    
    UIGraphicsBeginImageContextWithOptions(_bg.frame.size, NO, 0);
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, _bg.frame.size.width, _bg.frame.size.height) afterScreenUpdates:YES];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIColor *tintColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _bg.image = [newImage applyBlurWithRadius:5 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    CGRect tableFrame = _tableView.frame;
    tableFrame.size.height -= 68;
    tableFrame.origin.y += 2;
    _tableView.frame = tableFrame;
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setAllowsSelection:YES];
    [self.view addSubview:_tableView];
    
    _payeeView = [[UIView alloc] initWithFrame:self.view.bounds];
    _iconView = [[UIView alloc] initWithFrame:self.view.bounds];
    _categoryView = [[UIView alloc] initWithFrame:self.view.bounds];
    CGRect selectionViewFrame = _payeeView.frame;
    selectionViewFrame.size.height -= 140 + 216;
    selectionViewFrame.origin.y = 74.f;
    _payeeView.frame = selectionViewFrame;
    _iconView.frame = selectionViewFrame;
    _categoryView.frame = selectionViewFrame;
    
    //RESET FRAME ORIGIN TO 0 FOR USE WITH TABLEVIEW
    selectionViewFrame.origin.y = 0;
    
    _payeeTableView = [[UITableView alloc] initWithFrame:selectionViewFrame style:UITableViewStyleGrouped];
    _payeeTableView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [_payeeTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    _payeeTableView.delegate = self;
    _payeeTableView.dataSource = self;
    [_payeeTableView setAllowsSelection:YES];
    
    _categoryTableView = [[UITableView alloc] initWithFrame:selectionViewFrame style:UITableViewStyleGrouped];
    _categoryTableView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [_categoryTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    _categoryTableView.delegate = self;
    _categoryTableView.dataSource = self;
    [_categoryTableView setAllowsSelection:YES];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(14, 10, 12, 14);
    _iconCollectionView = [[UICollectionView alloc] initWithFrame:selectionViewFrame collectionViewLayout:layout];
    _iconCollectionView.delegate = self;
    _iconCollectionView.dataSource = self;
    
    [_iconCollectionView registerClass:[IconCollectionCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    _iconCollectionView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    
    CALayer *payeeBorder = [CALayer layer];
    payeeBorder.frame = CGRectMake(0, 0, _payeeTableView.frame.size.width, 2);
    payeeBorder.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor;
    CALayer *categoryBorder = [CALayer layer];
    categoryBorder.frame = CGRectMake(0, 0, _categoryTableView.frame.size.width, 2);
    categoryBorder.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor;
    
    [_categoryView addSubview:_categoryTableView];
    [_iconView addSubview:_iconCollectionView];
    [_payeeView addSubview:_payeeTableView];
    [_categoryView.layer addSublayer:categoryBorder];
    [_payeeView.layer addSublayer:payeeBorder];
    [self.view addSubview:_categoryView];
    [self.view addSubview:_iconView];
    [self.view addSubview:_payeeView];
    [_payeeView setHidden:YES];
    [_categoryView setHidden:YES];
    [_iconView setHidden:YES];
    
}

-(void)initDefaults{
    
    
    _reminderTypes = [NSArray arrayWithObjects:
                      @"On Due Date",
                      @"On Day Before",
                      @"Two Days Before",
                      @"The Week Before",
                      @"Two Weeks Before",
                      @"One Month Before", nil];
    _categoryCatalog = [NSArray arrayWithObjects:
                        @"Phone",
                        @"Family",
                        @"Car",
                        @"Fines",
                        @"House",
                        @"Utilities",
                        @"Education",
                        @"Entertainment",
                        @"Food",
                        @"Music",
                        @"Health",
                        @"Travel",
                        @"Transportation",
                        @"Fitness",
                        @"Business",
                        @"Office", nil];
    _categorySearchMatches = [_categoryCatalog mutableCopy];
    _iconCatalog = [NSArray arrayWithObjects:
                    @"money",
                    @"utilities",
                    @"car",
                    @"ticket",
                    @"house",
                    @"family",
                    @"phone", nil];
}

-(void)initNav{
    self.navigationItem.hidesBackButton = YES;
    
    UIView *navContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIImage *navLogoImage = [UIImage imageNamed:@"logo_nav_button"];
//    UIImage *navLogoImageDown = [UIImage imageNamed:@"logo_nav_button"];
    UIButton *navLogo = [[UIButton alloc] initWithFrame:CGRectMake(132, 0, 44, 44)];
    navLogo.tintColor = [UIColor grayColor];
    [navLogo setBackgroundImage:navLogoImage forState:UIControlStateNormal];
//    [navLogo setBackgroundImage:navLogoImageDown forState:UIControlStateHighlighted];
//    [navLogo addTarget:self action:@selector(showPopOver:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:navLogo];
    
    UIImage *backImage = [UIImage imageNamed:@"down_nav_button"];
//    UIImage *backImageDown = [UIImage imageNamed:@"down_nav_button"];
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0-5, 0, 44, 44)];
    backBtn.tintColor = [UIColor grayColor];
    [backBtn setBackgroundImage:backImage forState:UIControlStateNormal];
//    [backBtn setBackgroundImage:backImageDown forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(cancelCreate:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:backBtn];
    
    UIImage *createImage = [UIImage imageNamed:@"check_nav_button"];
//    UIImage *createImageDown = [UIImage imageNamed:@"check_nav_button"];
    UIButton *createBtn = [[UIButton alloc] initWithFrame:CGRectMake(265, 0, 44, 44)];
    createBtn.tintColor = [UIColor grayColor];
    [createBtn setBackgroundImage:createImage forState:UIControlStateNormal];
//    [reuseBtn setBackgroundImage:reuseImageDown forState:UIControlStateHighlighted];
    [createBtn addTarget:self action:@selector(create:) forControlEvents:UIControlEventTouchUpInside];
    [navContainer addSubview:createBtn];
    
    [self.navigationItem setTitleView:navContainer];
}

-(void)getPayees{
    // SNAG ALL OF THE PAYEE MODEL OBJECTS
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Payee"
                                              inManagedObjectContext:_context];
    
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:&error];
    
    _payees = [[NSMutableArray alloc] init];
    _payeeCatalog = [[NSMutableArray alloc] init];
    for (Payee *payee in fetchedObjects) {
        [_payees addObject:payee];
    }
    _payeeSearchMatches = [_payees mutableCopy];
}
@end
