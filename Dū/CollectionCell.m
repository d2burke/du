//
//  CollectionCell.m
//  CustomCollectionCell
//
//  Created by Daniel Burke on 12/5/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "CollectionCell.h"

@implementation CollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 145, 149)];
        topView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        [self addSubview:topView];
        
        UIView *botView = [[UIView alloc] initWithFrame:CGRectMake(0, 150, 145, 29)];
        botView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        [self addSubview:botView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
