//
//  NotesCell.m
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "NotesCell.h"

@implementation NotesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect cellFrame = self.frame;
        cellFrame.size.height = 120;
        _notesTextView.tag = 1;
        _notesTextView = [[UITextView alloc] initWithFrame:cellFrame];
        _notesTextView.backgroundColor = [UIColor whiteColor];
        _notesTextView.text = @"Type Note";
        _notesTextView.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _notesTextView.textColor = [UIColor grayColor];
        [self addSubview:_notesTextView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
