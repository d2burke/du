//
//  IconCollectionCell.m
//  Dū
//
//  Created by Daniel Burke on 12/15/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "IconCollectionCell.h"

@implementation IconCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 44, 44)];
        [self addSubview:_icon];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
