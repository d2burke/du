//
//  Note.h
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bill, RBill;

@interface Note : NSManagedObject

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * synced;
@property (nonatomic, retain) Bill *bill;
@property (nonatomic, retain) RBill *rbill;
@property (nonatomic, retain) NSManagedObject *user;

@end
