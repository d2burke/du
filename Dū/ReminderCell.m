//
//  ReminderCell.m
//  Dū
//
//  Created by Daniel Burke on 12/24/13.
//  Copyright (c) 2013 D2. All rights reserved.
//

#import "ReminderCell.h"

@implementation ReminderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _reminderTypes = [NSArray arrayWithObjects:
                         @"On Due Date",
                         @"Day Before",
                         @"Two Days Before",
                         @"Week Before",
                         @"Two Weeks Before",
                         @"One Month Before", nil];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        CGRect cellFrame = self.frame;
        _scrollView = [[UIScrollView alloc] initWithFrame:cellFrame];
        _scrollView.contentSize = CGSizeMake(321, 44);
        _scrollView.delegate = self;
        
        cellFrame.size.height = 44;
        _cellView = [[UIView alloc] initWithFrame:cellFrame];
        _cellView.backgroundColor = [UIColor whiteColor];
        
        _deleteView = [[UIView alloc] initWithFrame:cellFrame];
        _deleteView.backgroundColor = [UIColor redColor];
        _deleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 0, 80, 44)];
        _deleteLabel.textColor = [UIColor whiteColor];
        _deleteLabel.text = @"DELETE";
        _deleteLabel.textAlignment = NSTextAlignmentRight;
        _deleteLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _deleteView.alpha = 0;
        [_deleteView setHidden:YES];
        
        _reminderIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        _reminderIcon.image = [UIImage imageNamed:@"clock_cell_icon"];
        
        _reminderButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _reminderButton.frame = CGRectMake(50, 0, 280, 44);
        [_reminderButton setTitle:@"Add Reminder" forState:UIControlStateNormal];
        [_reminderButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _reminderButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _reminderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        _reminderLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 100, 10)];
        _reminderLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
        _reminderLabel.textColor = [UIColor grayColor];
        [_reminderLabel setHidden:YES];
        
        [_scrollView addSubview:_cellView];
        [_scrollView addSubview:_reminderIcon];
        [_scrollView addSubview:_reminderLabel];
        [_scrollView addSubview:_reminderButton];
        [_deleteView addSubview:_deleteLabel];
        [_scrollView addSubview:_deleteView];
        [self addSubview:_scrollView];
    }
    return self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%0.2f", scrollView.contentOffset.x);
    if(scrollView.contentOffset.x > 0){
        _deleteView.hidden = NO;
        _deleteView.alpha = scrollView.contentOffset.x/90;
    }
    else{
        _deleteView.hidden = YES;
        _deleteView.alpha = 0;
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(scrollView.contentOffset.x >= 80){
        NSIndexPath *indexPath = [(UITableView*)[self.superview superview] indexPathForCell:self];
        [_delegate didDeleteReminder:indexPath];
        [scrollView setContentOffset:CGPointMake(80, 0) animated:NO];
    }
}

-(void)layoutSubviews{

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
