//
//  Reminder.m
//  Dū
//
//  Created by Daniel Burke on 1/1/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "Reminder.h"
#import "Bill.h"


@implementation Reminder

@dynamic date;
@dynamic synced;
@dynamic bill;

@end
